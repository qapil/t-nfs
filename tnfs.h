/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	tnfs.h - globalni definitions, declarations and constants
 */
 
#ifndef	TNFS_H
#define	TNFS_H

#define TNFS_VERSION	1
#define	TNFS_PORT	0xBAFF

#define	TNFS_ROOT_INO	2
#define	PACKETSIZE	2048
#define MAXUSERNAMELEN	20
#define	MAXPASSWORDLEN	20
#define MAXNAMELEN	255

#ifdef	MAXPATHLEN
#  if MAXPATHLEN < 1000
#    undef MAXPATHLEN
#    define MAXPATHLEN	1000
#  endif
#else
#  define MAXPATHLEN	1000
#endif

#define	TRUE		1
#define FALSE		0

#define TNFS_IOC_GETMOUNTUID    _IOR( 'w', 1, __kernel_uid_t )

/* commands */
#define TNFS_CMD_OK		0x01
#define TNFS_CMD_FAIL		0x02
#define TNFS_CMD_AUTH		0x03
#define TNFS_CMD_ROOTDEV	0x04
#define TNFS_CMD_TERMINATE	0x05
#define TNFS_CMD_DISCONNECT	0x06
#define TNFS_CMD_MTIME		0x07
#define TNFS_CMD_READDIR	0x08
#define TNFS_CMD_LOOKUP		0x09
#define TNFS_CMD_READLINK	0x0A
#define TNFS_CMD_CREATE		0x0B
#define TNFS_CMD_SYMLINK	0x0C
#define	TNFS_CMD_OPEN		0x0D
#define	TNFS_CMD_CLOSE		0x0E
#define	TNFS_CMD_READ		0x0F
#define	TNFS_CMD_WRITE		0x10
#define TNFS_CMD_CHMOD		0x11
#define TNFS_CMD_LINK		0x12
#define TNFS_CMD_UNLINK		0x13
#define TNFS_CMD_RENAME		0x14
#define TNFS_CMD_TRUNC		0x15
#define TNFS_CMD_RMDIR		0x16
#define TNFS_CMD_STATFS		0x17
#define TNFS_MAX_CMD		0x17

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <sys/types.h>
#endif

struct tnfs_mount_data {
    int version;
    int uid;
    int gid;
    int server;
    pid_t umnt_pid;
};

struct tnfs_auth {
    uid_t uid;
    gid_t gid;
    uid_t nobody_uid;
    gid_t nobody_gid;
    char user[MAXUSERNAMELEN];
    char pass[MAXPASSWORDLEN];
};

typedef unsigned char BYTE;

#endif	/* TNFS_H */
