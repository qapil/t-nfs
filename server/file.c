/*
 *	        T N F S  - Server
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	file.c
 */

#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <fcntl.h>
#include <unistd.h>

#include "../tnfs.h"
#include "../tnfs_fn.h"
#include "server.h"

extern char path[];

void do_open( void )
{
    int mode = *(int*)(buf+4);
    short len = *(short*)(buf+8);
    char *request = buf+10;
    int fd;
    
    request[len] = '\0';

#ifdef DEBUG    
    printf( "OPEN (%.2x): %s", TNFS_CMD_OPEN, request);
#endif

    strcpy(path, server_root);
    strcat(path, request);

    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_OPEN, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }

    fd = open( path, mode );
    if ( fd == -1 ) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
    }
    else {
	buf[4] = 1;
	*(int*)(buf+5) = fd;
#ifdef DEBUG    
	printf( " - OK\n" );
#endif
    }
    send_packet( client, TNFS_CMD_OPEN, buf, 5 );
}

void do_close( void )
{
    int fd = *(int*)(buf+4);
    
#ifdef DEBUG    
    printf( "CLOSE (%.2x): %i", TNFS_CMD_CLOSE, fd );
#endif

    if ( close( fd ) == -1 ) {
	buf[4] = 0;
	*(int*)(buf+5) = errno; 
	send_packet( client, TNFS_CMD_CLOSE, buf, 5 );
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
    }
    else {
	buf[4] = 1;
	send_packet( client, TNFS_CMD_CLOSE, buf, 1 );
#ifdef DEBUG    
	printf( " - OK\n" );
#endif
    }
}

void do_read( void )
{
    int fd = *(int*)(buf+4);
    int off = *(int*)(buf+8);
    int len = *(int*)(buf+12);
    
#ifdef DEBUG    
    printf( "READ (%.2x): fd=%i, off=%i, len=%i ", TNFS_CMD_READ, fd, off, len );
#endif

    if ( lseek( fd, off, SEEK_SET ) == -1
	|| ( len = read( fd, buf + 9, len ) ) == -1 ) {
#ifdef DEBUG    
	printf( "FAILED(%i)\n", errno );
#endif
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	len = 0;
    }
    else {
#ifdef DEBUG    
	printf( "OK\n" );
#endif
	buf[4] = 1;
	*(int*)(buf+5) = len;
    }
    send_packet( client, TNFS_CMD_READ, buf, len+5 );
}

void do_write(void)
{
    int fd = *(int*)(buf+4);
    int off = *(int*)(buf+8);
    int len = *(int*)(buf+12);
    
#ifdef DEBUG    
    printf("WRITE (%.2x): fd=%i, off=%i, len=%i ", TNFS_CMD_WRITE, fd, off, len);
#endif

    if (lseek(fd, off, SEEK_SET) == -1
	|| (len = write(fd, buf+16, len)) == -1) {
#ifdef DEBUG    
	printf("FAILED(%i)\n", errno);
#endif
	buf[4] = 0;
	*(int*)(buf+5) = errno; 
    }
    else {
#ifdef DEBUG    
	printf( "OK\n" );
#endif
        buf[4] = 1;
	*(int*)(buf+5) = len;
    }
    send_packet(client, TNFS_CMD_WRITE, buf, 5);
}

void do_link( void )
{
    short len1 = *(short*)(buf+4);
    char *oldpath = buf+6;
    short len2 = *(short*)(buf+6+len1);
    char *newpath = buf+8+len1;
    char str[MAXPATHLEN];    

    oldpath[len1] = newpath[len2] = '\0';

#ifdef DEBUG
    printf("LINK (%.2x): %s --> %s", TNFS_CMD_LINK, newpath, oldpath);
#endif

    strcpy(str, server_root);
    strcat(str, newpath);
    strcpy(path, server_root);
    strcat(path, oldpath);
    
    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_LINK, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }

    if (link(path, str) == -1 ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_LINK, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }

    buf[4] = 1;
    send_packet( client, TNFS_CMD_LINK, buf, 1);
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_unlink( void )
{
    short len = *(short*)(buf+4);
    char *request = buf+6;

    request[len] = 0;

#ifdef DEBUG
    printf("UNLINK (%.2x): %s", TNFS_CMD_UNLINK, request);
#endif
    
    strcpy(path, server_root);
    strcat(path, request);
    
    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_UNLINK, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }

    if (unlink(path) == -1 ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_UNLINK, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }

    buf[4] = 1;
    send_packet( client, TNFS_CMD_UNLINK, buf, 1);
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_trunc( void )
{
    long size = *(long*)(buf+4);
    short len = *(short*)(buf+8);
    char *request = buf+10;

    request[len] = 0;

#ifdef DEBUG
    printf("TRUNC (%.2x): %s, size=%ld", TNFS_CMD_TRUNC, request, size);
#endif
    
    strcpy(path, server_root);
    strcat(path, request);

    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_TRUNC, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }
    
    if (truncate(path, size) == -1 ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_TRUNC, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }

    buf[4] = 1;
    send_packet( client, TNFS_CMD_TRUNC, buf, 1);
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}
