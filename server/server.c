/*
 *	        T N F S  - Server
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	server.c
 *
 *	server pro ntfs
 */

#include <sys/socket.h>		// socket(), connect()
#include <sys/types.h>
#include <netdb.h>		// gethostbyname() & spol.
#include <netinet/in.h>		// struct sockaddr_in
#include <sys/time.h>		// struct timeval
#include <sys/param.h>		// MAXPATHLEN
#include <stdio.h>
#include <stdlib.h>		// atoi()
#include <string.h>		// memset()
#include <errno.h>		// errno
#include <unistd.h>		// fork(), gethostname() & spol.
#include <sys/wait.h>		// waitpid()
#include <signal.h>		// signal()
#include <fcntl.h>		// fcntl()
#include <sys/stat.h>		// struct stat
#include <sys/vfs.h>		// statfs

#include "../tnfs.h"
#include "../tnfs_fn.h"
#include "server.h"
#include "dir.h"
#include "symlink.h"
#include "file.h"

int sock;			// soket pro prijem
int client;			// deskriptor pro komunikaci s klientem
int port;			// naslouchaci port serveru
struct sockaddr_in s_in;	// adresa klienta
int addrlen;
char host[MAXNAMELEN+1];	// jmeno pocitace
struct hostent * hostentry;

int main_server;
int server_id;			// kolikaty server je uz spusten
int childs;			// kolik je aktivnich servru

char user[MAXUSERNAMELEN+1],
     pass[MAXPASSWORDLEN+1];

char server_root[MAXPATHLEN+1];		// koren serverem zpristupnenych souboru
unsigned char buf[PACKETSIZE+4];	// prijata a odesilana data
char path[MAXPATHLEN+1];		// na vytvoreni cesty

struct tnfs_auth auth;
char config_file[MAXNAMELEN+1];
struct stat config_stat;
unsigned root_dev;

//FILE * logstr=stderr;		// stream, kam se budou posilat logy

#define	printlog	printf

int check_config( char *path )
{
    struct stat st;
    
    if (stat( path, &st ) || st.st_dev != config_stat.st_dev
	|| st.st_ino != config_stat.st_ino)
	return 0;	
    else
	return -1;
}

unsigned long invent_inos(unsigned long n)
{
    static unsigned long ino = 2;
    
    if (ino + 2*n < ino)
	ino = 2;
	
    ino += n;
    return ino;
}

void usage( void )
{
    printf( "usage:	tnfsd [-f config_file]\n" );
    exit( -1 );
}

void parse_config( int child )
{
    FILE *fp;
    int i;

    port = 0;    
    if ((fp = fopen( config_file, "r")) == NULL) {
	perror("tnfsd");
	exit( -1 );
    }

    if (fscanf(fp, "%X\n", &port) == EOF || port == 0) {
        printf("Bad config file format, see man page :-)\n");
	fclose(fp);
        exit( -2 );
    }

    if (!child)
	printf( "tnfs_server v. 0.6-1 on port 0x%X (%d)\n\n", port, port );
    else {
	do {
	    i = fscanf(fp, "%s %s %s\n", user, pass, server_root);
#ifdef DEBUG
	    printf("%s, %s, %s\n", user, pass, server_root);
#endif	    
	} while ( i != EOF && strcmp(user, auth.user) );
	stat(server_root, &config_stat);
	root_dev = 0;
    }
    
    fclose(fp);
    stat(config_file, &config_stat);
}

void parse_params( int argc, char * argv[] )
{
    strcpy( config_file, "conf.tnfs");
    
    if (*(++argv) && strcmp(*argv, "-f") == 0) {
	strncpy( config_file, *(++argv), MAXNAMELEN );
	argc -= 2;
    }
    else if (argc > 1) usage();

    parse_config( FALSE );
}

/* reakce na signaly */
void sig_handler( int SIGNUM )
{
    signal( SIGNUM, sig_handler );
    
    /* v budoucnu by to melo umet kilnout synovske procesy */
    switch ( SIGNUM ) {
    case SIGINT :
    case SIGQUIT :
    case SIGTERM :
	// ukoncime cinnost synovskych procesu i sebe
	if ( !main_server ) {
	    send_packet( client, TNFS_CMD_TERMINATE, buf, 0 );
	    shutdown( client, 2 );
	    close(client);
	}
	else {
	    int i=2;
	    close(sock);
	    while ((i=sleep(i)) != 0);
	}
        exit(0);
	break;
    case SIGHUP :
	// casem to mozna bude neco delat
	break;
    case SIGCHLD : { pid_t pid;
	childs--;
	pid = waitpid( -1, 0, WNOHANG );
	printf( "Server %d skoncil cinnost\n", pid ); }
	break;
    default:
	// ostatni signaly jsou nezajimave, jen nas nemusi
	// ukoncovat;
    }
}

void do_statfs(void)
{
    struct statfs st;
    
#ifdef DEBUG
    printlog("STATFS (%.2x)\n", TNFS_CMD_STATFS);
#endif

    if (statfs(server_root, &st) == -1) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_STATFS, buf, 5 );
	return;
    }

    buf[4] = 1;
    *(long*)(buf+5) = st.f_blocks;
    *(long*)(buf+9) = st.f_bfree;
    *(long*)(buf+13) = st.f_bavail;
    send_packet( client, TNFS_CMD_STATFS, buf, 13 );
}

void do_auth(void)
{
    auth = *(struct tnfs_auth *)(buf+4);
#ifdef DEBUG
    printf("AUTH (%.2x)\n", TNFS_CMD_AUTH);
    printf("\tuid=%i\n\tgid=%i\n\tnobody_uid=%i\n\tnobody_gid=%i\n",
	    auth.uid, auth.gid, auth.nobody_uid, auth.nobody_gid);
    printf("\tusername=%s\n\tpassword=%s\n", auth.user, auth.pass);
#endif
    parse_config(TRUE);
    if (strcmp(user, auth.user) == 0 && strcmp(pass, auth.pass) == 0)
        send_packet( client, TNFS_CMD_OK, buf, 0 );
    else
        send_packet( client, TNFS_CMD_FAIL, buf, 0 );
}

void do_rootdev(void)
{
    root_dev = *(unsigned*)(buf+4);
#ifdef DEBUG
    printf("ROOTDEV (%.2x): %u\n", TNFS_CMD_ROOTDEV, root_dev);
#endif
    send_packet(client, TNFS_CMD_ROOTDEV, buf, 0);
}

void unknown(void)
{
#ifdef DEBUG
    fprintf(stderr, "UNKNOWN COMMAND: %.2x\n", (int) *buf);
#endif
    send_packet( client, TNFS_CMD_FAIL, buf, 0 );
}

int read_request(int sock, void *buf)
{
    unsigned char *b = buf;
    int len;
    
    if ((len = read_sock(sock, b, 4)) < 0)		// precti hlavicku (4B)
	return len;
    len = b[1] << 16;					// zjisti delku dat
    len += b[2] << 8;
    len += b[3];

    if (len) len = read_sock(sock, b+4, len);		// precti data
    
    return len;
}

void doRequest( void )
{
    while (1) {
	if (read_request(client, buf) < 0) {
	    fprintf(stderr, "error while receiving data\n");
	    exit(0);
	}
	
	switch (buf[0]) {
	    case TNFS_CMD_AUTH: 
		do_auth();
		break;
	    case TNFS_CMD_ROOTDEV: 
		do_rootdev();
		break;
	    case TNFS_CMD_TERMINATE:
#ifdef DEBUG
		printf("TERMINATE\n");
#endif		
	        return;
	    case TNFS_CMD_MTIME:
		do_mtime();
		break;
	    case TNFS_CMD_READDIR:
		do_readdir();
		break;
	    case TNFS_CMD_LOOKUP:
		do_lookup();
		break;
	    case TNFS_CMD_READLINK:
		do_readlink();
		break;
	    case TNFS_CMD_CREATE:
		do_create();
		break;
	    case TNFS_CMD_SYMLINK:
		do_symlink();
		break;
	    case TNFS_CMD_OPEN:
		do_open();
		break;
	    case TNFS_CMD_CLOSE:
		do_close();
		break;
	    case TNFS_CMD_READ:
		do_read();
		break;
	    case TNFS_CMD_WRITE:
		do_write();
		break;
	    case TNFS_CMD_CHMOD:
		do_chmod();
		break;
	    case TNFS_CMD_LINK:
		do_link();
		break;
	    case TNFS_CMD_UNLINK:
		do_unlink();
		break;
	    case TNFS_CMD_RENAME:
		do_rename();
		break;
	    case TNFS_CMD_TRUNC:
		do_trunc();
		break;
	    case TNFS_CMD_RMDIR:
		do_rmdir();
		break;
	    case TNFS_CMD_STATFS:
		do_statfs();
		break;
	    default:
		unknown();
		break;
	}
    }
}

/* 
  funkce ceka na pripojeni nejakeho klienta, az se jeden najde,
  forkne se, preda obsluhu klienta synovi a bude pokracovat
  v odposlechu klientu
 */
void wait_for_request( void )
{
    while ( 1 ) {
        if (listen( sock, 1 )) {	// cekame na klienta
	    perror("listen");
	    break;
	}

	printlog( "Server listening\n");

	addrlen = sizeof( struct sockaddr );
        client = accept( sock, &s_in, &addrlen );	// jeden se dovolal
    
        if ( client == -1 ) {
	    perror("accept");
	    break;
	}
	server_id++;
	printlog( "Server %d started.\n", server_id );

	doRequest();

	shutdown(client, 2);
	close(client);

	printlog( "Server %d finished.\n", server_id );
	childs++;	// zase jeden
    }
}

int main( int argc, char * argv[] )
{
    main_server = 1;
    server_id = 0;		// hlavni server
#ifdef	DO_LOGS
    // schrasti soubor pro zapisy
#endif
    if ( gethostname( host, MAXNAMELEN ) == -1 ) {
	printlog( "Error calling gethostname(): %d\n", errno );
	exit( -1 );
    }
    if ( (hostentry = gethostbyname( host )) == 0 ) {
	printlog( "Error calling gethostbyname(): %d\n", h_errno );
	exit( -1 );
    }
    if ( (sock = socket( PF_INET, SOCK_STREAM, 0 )) < 0 ) {
	printlog( "Error calling socket(): %d\n", errno );
	exit( -1 );
    }

    parse_params( argc, argv );
    
    memset( &s_in, 0, sizeof( s_in ) );
    s_in.sin_family = AF_INET;
    s_in.sin_port = port;
    memcpy( &s_in.sin_addr, hostentry -> h_addr, hostentry -> h_length );
    if ( bind( sock, (struct sockaddr *)&s_in, sizeof( s_in ) ) ) {
	printlog( "Error calling bind(): %d\n", errno );
	perror("bind");
	exit( -1 );
    }
    childs = 0;
    
    signal( SIGTERM, sig_handler );
    signal( SIGHUP, sig_handler );
    signal( SIGINT, sig_handler );
    signal( SIGQUIT, sig_handler );
    signal( SIGUSR1, sig_handler );
    signal( SIGUSR2, sig_handler );
    signal( SIGCHLD, sig_handler );
    
    wait_for_request();			// jdem poslouchat
    return 0;
}
