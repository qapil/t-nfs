/*
 *	        T N F S  - Server
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	symlink.c
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/stat.h>

#include "../tnfs.h"
#include "../tnfs_fn.h"
#include "server.h"
#include "symlink.h"

extern char path[];

void do_readlink( void )
{
    int buflen = *(int*)(buf+4);
    short len = *(short*)(buf+8);
    char *request = buf + 10;

    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);
#ifdef DEBUG
    printf("READLINK (%.2x): %s", TNFS_CMD_READLINK, request);
#endif
    if ( (len = readlink( path, buf+7, buflen )) == -1 ) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_READLINK, buf, 5 );
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }
    buf[4] = 1;
    *(short*)(buf+5) = len;
    send_packet( client, TNFS_CMD_READLINK, buf, 3 + len );
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_symlink( void )
{
    short len1 = *(short*)(buf+4);
    char *oldpath = buf+6;
    short len2 = *(short*)(buf+6+len1);
    char *newpath = buf+8+len1;
    struct stat st;

    oldpath[len1] = newpath[len2] = '\0';

    strcpy(path, server_root);
    strcat(path, newpath);
    
#ifdef DEBUG
    printf("SYMLINK (%.2x): %s --> %s", TNFS_CMD_SYMLINK, path, oldpath);
#endif

    if (symlink(oldpath, path) == -1 || lstat( path, &st ) ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_SYMLINK, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }

    buf[4] = 1;
//    *(long*)(buf+5) = st.st_ino;
    *(long*)(buf+5) = (long)invent_inos(1);
    *(unsigned*)(buf+9) = st.st_mode;
    *(unsigned*)(buf+13) = st.st_nlink;
    if ( st.st_uid == getuid() )
        *(unsigned*)(buf+17) = (unsigned)auth.uid;
    else *(unsigned*)(buf+17) = (unsigned)auth.nobody_uid;
    if ( st.st_gid == getgid() )
        *(unsigned*)(buf+21) = (unsigned)auth.gid;
    else *(unsigned*)(buf+21) = (unsigned)auth.nobody_gid;
    *(long*)(buf+25) = st.st_size;
    *(long*)(buf+29) = st.st_atime;
    *(long*)(buf+33) = st.st_mtime;
    *(long*)(buf+37) = st.st_ctime;
    send_packet( client, TNFS_CMD_SYMLINK, buf, 37 );
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}
