/*
 *	        T N F S  - Server
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	dir.c
 */

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/errno.h>
#include <fcntl.h>
 
#include "../tnfs.h"
#include "../tnfs_fn.h"
#include "server.h"
#include "dir.h"

extern char path[];
extern int errno;

void do_mtime(void)
{
    short len = *(short*)(buf+4);
    char *request = buf+6;
    struct stat st;
    
    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);
#ifdef DEBUG
    printf("MTIME (%.2x): %s ", TNFS_CMD_MTIME, request);
#endif

    if (stat(path, &st))
    {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_MTIME, buf, 5 );
#ifdef DEBUG
	printf("- FAILED\n");
#endif
    }
    else {
	buf[4] = 1;
	*(long*)(buf+5) = st.st_mtime;
	send_packet( client, TNFS_CMD_MTIME, buf, 5 );
#ifdef DEBUG
	printf("- OK\n");
#endif
    }
}

void do_readdir(void)
{
    short len = *(short*)(buf+4);
    char *request = buf+6;
    DIR * dirp;
    struct dirent * de;
    
    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);

#ifdef DEBUG    
    printf( "READDIR (%.2x): %s", TNFS_CMD_READDIR, request );
#endif
    
    if ( ( dirp = opendir( path ) ) == NULL ) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_READDIR, buf, 5 );
    } else {
	while ((de = readdir( dirp )) != NULL ) {
	    if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
		continue;
		
	    buf[4] = 1;			// vratime adr. polozku
//	    *(unsigned long*)(buf+5) = de -> d_ino;
	    *(unsigned long*)(buf+5) = invent_inos(1);
	    *(short*)(buf+9) = len = strlen( de -> d_name );
	    strncpy( buf + 11, de -> d_name, len);
	    send_packet( client, TNFS_CMD_READDIR, buf, 7 + len );
	}

	buf[4] = 2;	// konec vypisu
	send_packet( client, TNFS_CMD_READDIR, buf, 1 );

	closedir(dirp);
    }
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_lookup(void)
{
    short len = *(short*)(buf+4);
    char *request = buf+6;
    struct stat st;
    
    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);

#ifdef DEBUG    
    printf("LOOKUP (%.2x): %s", TNFS_CMD_LOOKUP, request);
#endif

    if ( lstat( path, &st ) ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_LOOKUP, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
    } else {
	buf[4] = 1;
//	*(long*)(buf+5) = st.st_ino;
	if (/*st.st_ino == TNFS_ROOT_INO &&*/ (unsigned)st.st_dev == root_dev)
	    *(long*)(buf+5) = TNFS_ROOT_INO;
	else
	    *(long*)(buf+5) = (long)invent_inos(1);
	*(unsigned*)(buf+9) = st.st_mode;
	*(unsigned*)(buf+13) = st.st_nlink;
	if ( st.st_uid == getuid() )
	    *(unsigned*)(buf+17) = (unsigned)auth.uid;
	else *(unsigned*)(buf+17) = (unsigned)auth.nobody_uid;
	if ( st.st_gid == getgid() )
	    *(unsigned*)(buf+21) = (unsigned)auth.gid;
	else *(unsigned*)(buf+21) = (unsigned)auth.nobody_gid;
	*(long*)(buf+25) = st.st_size;
	*(long*)(buf+29) = st.st_atime;
	*(long*)(buf+33) = st.st_mtime;
	*(long*)(buf+37) = st.st_ctime;
	send_packet( client, TNFS_CMD_LOOKUP, buf, 37 );
#ifdef DEBUG    
	printf( " - OK\n" );
#endif
    }
}

void do_create( void )
{
    int mode = *(int*)(buf+4);
    short len = *(short*)(buf+8);
    char *request = buf+10;
    struct stat st;
    int fd;

    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);
#ifdef DEBUG
    printf("CREATE (%.2x): %s", TNFS_CMD_CREATE, request);
#endif
    
    if ( mode & S_IFDIR )
	fd = mkdir( path, mode );
    else
	if ((fd = creat( path, mode )) != -1)
	    close(fd);

    if ( fd == -1 || stat( path, &st ) ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_CREATE, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }
    
    buf[4] = 1;
//    *(long*)(buf+5) = st.st_ino;
    *(long*)(buf+5) = (long)invent_inos(1);
    *(unsigned*)(buf+9) = st.st_mode;
    *(unsigned*)(buf+13) = st.st_nlink;
    if ( st.st_uid == getuid() )
        *(unsigned*)(buf+17) = (unsigned)auth.uid;
    else *(unsigned*)(buf+17) = (unsigned)auth.nobody_uid;
    if ( st.st_gid == getgid() )
        *(unsigned*)(buf+21) = (unsigned)auth.gid;
    else *(unsigned*)(buf+21) = (unsigned)auth.nobody_gid;
    *(long*)(buf+25) = st.st_size;
    *(long*)(buf+29) = st.st_atime;
    *(long*)(buf+33) = st.st_mtime;
    *(long*)(buf+37) = st.st_ctime;
    send_packet( client, TNFS_CMD_CREATE, buf, 37 );
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_chmod( void )
{
    short len = *(short*)(buf+6);
    char *request = buf+8;

#ifdef DEBUG    
    printf("CHMOD (%.2x): %s", TNFS_CMD_CHMOD, request);
#endif

    request[len] = '\0';

    strcpy(path, server_root);
    strcat(path, request);

    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_CHMOD, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }

    if ( chmod( path, *(short*)(buf+4) ) == -1 ) {
	buf[4] = 0;
        *(int*)(buf+5) = errno;
	send_packet( client, TNFS_CMD_CHMOD, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }

    buf[4] = 1;
    send_packet( client, TNFS_CMD_CHMOD, buf, 1 );
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_rename( void )
{
    short len1 = *(short*)(buf+4);
    char *oldpath = buf+6;
    short len2 = *(short*)(buf+6+len1);
    char *newpath = buf+8+len1;
    char str[MAXPATHLEN];    

    oldpath[len1] = newpath[len2] = '\0';

#ifdef DEBUG
    printf("RENAME (%.2x): %s --> %s", TNFS_CMD_RENAME, oldpath, newpath);
#endif

    strcpy(str, server_root);
    strcat(str, newpath);
    strcpy(path, server_root);
    strcat(path, oldpath);

    if ( check_config(path) ) {
	buf[4] = 0;
        *(int*)(buf+5) = EACCES;
	send_packet( client, TNFS_CMD_RENAME, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL (SERVER CONFIG FILE)\n" );
#endif
	return;
    }
    
    if (rename(path, str) == -1) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet(client, TNFS_CMD_RENAME, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }
    
    buf[4] = 1;
    send_packet(client, TNFS_CMD_RENAME, buf, 1);
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}

void do_rmdir( void )
{
    short len = *(short*)(buf+4);
    char *request = buf+6;

    request[len] = '\0';

#ifdef DEBUG
    printf("RMDIR (%.2x): %s", TNFS_CMD_RMDIR, request);
#endif

    strcpy(path, server_root);
    strcat(path, request);
    
    if (rmdir(path) == -1) {
	buf[4] = 0;
	*(int*)(buf+5) = errno;
	send_packet(client, TNFS_CMD_RMDIR, buf, 5);
#ifdef DEBUG    
	printf( " - FAIL\n" );
#endif
	return;
    }
    
    buf[4] = 1;
    send_packet(client, TNFS_CMD_RMDIR, buf, 1);
#ifdef DEBUG    
    printf( " - OK\n" );
#endif
}
