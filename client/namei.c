/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	namei.c
 */

#include <linux/fs.h>
#include <linux/mm.h>
#include <asm/uaccess.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "namei.h"
#include "support.h"
#include "net.h"
#include "inode.h"
#include "cache.h"
#include "dir.h"

int tnfs_create(struct inode *dir, struct dentry *dentry, int mode)
{
    struct inode *inode;
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len;
    struct tnfs_stat st;
    int res, error = 0;
    
#ifdef DEBUG
    printk("tnfs_create: %s, mode=%o\n", dentry->d_name.name, mode);
#endif

    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(int*)(buf) = mode;
    *(short*)(buf+4) = len = tnfs_build_path( dentry, buf + 6 );
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }

    res = tnfs_request(TNFS_SB( dir->i_sb ), TNFS_CMD_CREATE, 6+len);
    
    if ( res < 0 )
	error = res;
    else if ( buf[0] > 1 )
	error = -EIO;
    else if ( buf[0] == 0 ) 
	error = -*(int*)(buf+1);
    else
	memcpy( &st, buf+1, sizeof( struct tnfs_stat ) );

    tnfs_unlock(TNFS_SB(dir->i_sb));
    if (error) return error;

    inode = tnfs_iget( dir -> i_sb, &st );
    if ( inode == NULL ) return -EACCES;
    
    /* mtime = 0  ->  zavola se initialise_dir_cache() */
    get_dir_cache( dir -> i_dev, st.st_ino, 0, NULL );
    
    d_instantiate(dentry, inode);
    return 0;
}

int tnfs_link(struct dentry *old_dentry, struct inode *dir, struct dentry *dentry)
{
    struct inode *inode = old_dentry->d_inode;
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len1, len2;
    int res, error = 0;
    
#ifdef DEBUG
    printk("tnfs_link: %s -> %s\n", dentry->d_name.name, old_dentry->d_name.name);
#endif

    if (S_ISDIR(inode->i_mode))
	return -EPERM;

    d_drop(dentry);
    
    /* mtime = 0  ->  zavola se initialise_dir_cache() */
    get_dir_cache(dir->i_dev, dir->i_ino, 0, NULL);
    
    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = len1 = tnfs_build_path(old_dentry, buf+2);
    if (len1 == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    *(short*)(buf+2+len1) = len2 = tnfs_build_path(dentry, buf+4+len1);
    if (len2 == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    res = tnfs_request(TNFS_SB( dir->i_sb ), TNFS_CMD_LINK, 4+len1+len2);
    
    if ( res < 0 )
	error = res;
    else if ( buf[0] > 1 )
	error = -EIO;
    else if ( buf[0] == 0 )
	error = -*(int*)(buf+1);

    tnfs_unlock(TNFS_SB(dir->i_sb));
    if (error) return error;

    inode->i_nlink ++;
    mark_inode_dirty(inode);
//    d_instantiate( dentry, inode );
    
    return 0;
}

int tnfs_unlink(struct inode *dir, struct dentry *dentry)
{
    struct inode *inode = dentry->d_inode;
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_unlink: %s, nlink=%d\n", dentry->d_name.name, inode->i_nlink);
#endif

    if (S_ISDIR(inode->i_mode))
	return -EPERM;
    
    /* mtime = 0  ->  zavola se initialise_dir_cache() */
    get_dir_cache(dir->i_dev, dir->i_ino, 0, NULL);
    
    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = len = tnfs_build_path(dentry, buf+2);
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    res = tnfs_request(TNFS_SB( dir->i_sb ), TNFS_CMD_UNLINK, 2+len);
    
    if ( res < 0 )
	error = res;
    else if ( buf[0] > 1 )
	error = -EIO;
    else if ( buf[0] == 0 ) 
	error = -*(int*)(buf+1);

    tnfs_unlock(TNFS_SB(dir->i_sb));
    if (error) return error;

    tnfs_renew_times(dentry);
    mark_inode_dirty(inode);

    if (--inode->i_nlink == 0)
	d_delete(dentry);

    return 0;
}

int tnfs_symlink(struct inode *dir, struct dentry *dentry, const char *symname)
{
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len1, len2;
    struct tnfs_stat st;
    struct inode *inode;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_symlink: %s --> %s\n", symname, dentry->d_name.name);
#endif

    if ((len1 = strlen(symname)) > MAXPATHLEN)
	return -ENAMETOOLONG;

    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = len1;
    strcpy(buf+2, symname);
    *(short*)(buf+2+len1) = len2 = tnfs_build_path( dentry, buf+4+len1 );
    if (len2 == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    res = tnfs_request( TNFS_SB( dir->i_sb ), TNFS_CMD_SYMLINK, 4+len1+len2);

    if ( res < 0 )
	error = res;
    else if (buf[0] > 1)
	error = -EIO;
    else if (buf[0] == 0)
	error = -*(int*)(buf+1);
    else
	memcpy( &st, buf+1, sizeof(struct tnfs_stat) );

    tnfs_unlock(TNFS_SB(dir->i_sb));
    if (error) return error;

    inode = tnfs_iget( dir->i_sb, &st );
    if (inode == NULL) return -EACCES;

    /* mtime = 0  ->  zavola se initialise_dir_cache() */
    get_dir_cache( dir -> i_dev, st.st_ino, 0, NULL );

    d_instantiate( dentry, inode );
    return 0;
}

int tnfs_mkdir(struct inode *dir, struct dentry *dentry, int mode)
{
    mode |= S_IFDIR;
    
#ifdef DEBUG
    printk("tnfs_mkdir: %s, mode=%o\n", dentry->d_name.name, mode);
#endif

    return tnfs_create(dir, dentry, mode);
}

int tnfs_rmdir(struct inode *dir, struct dentry *dentry)
{
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_rmdir: %s\n", dentry->d_name.name);
#endif

    if (! list_empty(&dentry->d_hash))
	return -EBUSY;

    // mtime = 0  ->  zavola se initialise_dir_cache()
    get_dir_cache( dir->i_dev, dir->i_ino, 0, NULL );
    
    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = len = tnfs_build_path( dentry, buf + 2 );
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    res = tnfs_request(TNFS_SB( dir->i_sb ), TNFS_CMD_RMDIR, 2+len);
    
    if ( res < 0 )
	error = res;
    else if ( buf[0] > 1 )
        error = -EIO;
    else if ( buf[0] == 0 )
	error = -*(int*)(buf+1);

    tnfs_unlock(TNFS_SB(dir->i_sb));
    
    return error;
}

int tnfs_rename(struct inode *old_dir, struct dentry *old_dentry, 
		struct inode *new_dir, struct dentry *new_dentry)
{
    unsigned char * buf = TNFS_SB( old_dir -> i_sb ) -> packet;
    short len1, len2;
    int res, error = 0;
    
#ifdef DEBUG
    printk("tnfs_rename: %s -> %s, ct=%d\n", old_dentry->d_name.name,
	    new_dentry->d_name.name, new_dentry->d_count);
#endif

    // mtime = 0  ->  zavola se initialise_dir_cache()
    get_dir_cache( old_dir->i_dev, old_dir->i_ino, 0, NULL );
    get_dir_cache( new_dir->i_dev, new_dir->i_ino, 0, NULL );
    
    tnfs_lock(TNFS_SB(old_dir->i_sb));
    
    *(short*)(buf) = len1 = tnfs_build_path(old_dentry, buf+2);
    if (len1 == -1) {
	tnfs_unlock(TNFS_SB(old_dir->i_sb));
	return -ENAMETOOLONG;
    }
    *(short*)(buf+2+len1) = len2 = tnfs_build_path(new_dentry, buf+4+len1);
    if (len2 == -1) {
	tnfs_unlock(TNFS_SB(old_dir->i_sb));
	return -ENAMETOOLONG;
    }

    res = tnfs_request(TNFS_SB(old_dir->i_sb), TNFS_CMD_RENAME, 4+len1+len2);
    if ( res < 0 )
	error = res;
    else if (buf[0] > 1)
	error = -EIO;
    else if (buf[0] == 0) 
	error = -*(int*)(buf+1);

    tnfs_unlock(TNFS_SB(old_dir->i_sb));
    if (error) return error;
    
    tnfs_renew_times(old_dentry);
    tnfs_renew_times(new_dentry);
    
    return 0;
}


int tnfs_notify_change( struct dentry * dentry, struct iattr * attr )
{
    struct inode * inode = dentry -> d_inode;
    unsigned char * buf = TNFS_SB( inode -> i_sb ) -> packet;
    short len, refresh = 0;
    int res, error;
    unsigned mask = ( S_IFREG | S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO 
		    | S_ISUID | S_ISGID | S_ISVTX );

    if ((error = inode_change_ok(inode, attr)) < 0)
	return error;
	
    error = -EPERM;
    
    if (attr->ia_valid & ATTR_SIZE) {
#ifdef	DEBUG
	printk( "tnfs_notify_change: file=%s, size=%d\n", 
	        dentry -> d_name.name, (int)attr->ia_size );
#endif
	tnfs_lock(TNFS_SB(inode->i_sb));
    
	*(long*)(buf) = attr -> ia_size;
	*(short*)(buf+4) = len = tnfs_build_path( dentry, buf + 6 );
	if (len == -1) {
	    tnfs_unlock(TNFS_SB(inode->i_sb));
	    return -ENAMETOOLONG;
	}

	res = tnfs_request(TNFS_SB( inode->i_sb ), TNFS_CMD_TRUNC, 6+len);
    
	if ( res < 0 ) 
	    error = res;
	else if ( buf[0] > 1 )
	    error = -EIO;
	else if ( buf[0] == 0 )
	    error = -*(int*)(buf+1);
	else error = 0;
	
	tnfs_unlock(TNFS_SB(inode->i_sb));
	if (error) return error;

	if (attr->ia_size < inode->i_size) {
	    truncate_inode_pages(inode, attr->ia_size);
	    inode->i_size = attr->ia_size;
	    refresh = 1;
	}
    }

    if ((attr->ia_valid & ATTR_MODE) && (attr->ia_mode & mask)) {
#ifdef	DEBUG
	printk( "tnfs_notify_change: file=%s, mode=0%o\n", 
		dentry -> d_name.name, attr -> ia_mode );
#endif
	tnfs_lock(TNFS_SB(inode->i_sb));

	*(short*)(buf) = attr -> ia_mode & mask;
	*(short*)(buf+2) = len = tnfs_build_path( dentry, buf + 4 );
	if (len == -1) {
	    tnfs_unlock(TNFS_SB(inode->i_sb));
	    return -ENAMETOOLONG;
	}

	res = tnfs_request(TNFS_SB( inode->i_sb ), TNFS_CMD_CHMOD, 4+len);
    
	if ( res < 0 ) 
	    error = res;
	else if ( buf[0] > 1 )
	    error = -EIO;
    	else if ( buf[0] == 0 )
	    error = -*(int*)(buf+1);
	else error = 0;

	tnfs_unlock(TNFS_SB(inode->i_sb));
	if (error) return error;

	inode_setattr(inode, attr);
        refresh = 1;
    }

    if (refresh)
	tnfs_renew_times(dentry);

    return error;
}
