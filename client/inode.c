/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	inode.c
 */

#include <linux/fs.h>
#include <linux/sched.h>	// CURRENT_TIME

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "inode.h"
#include "file.h"
#include "dir.h"
#include "symlink.h"

unsigned long tnfs_invent_inos(unsigned long n)
{
    static unsigned long ino = 2;
    
    if (ino + 2*n < ino) {
	ino = 2;
    }
    ino += n;
    return ino;
}

static struct tnfs_stat *tnfs_stat = NULL;
static struct semaphore read_semaphore = MUTEX;

struct inode *tnfs_iget(struct super_block *sb, struct tnfs_stat *st)
{
    struct inode *result;

    down(&read_semaphore);
    tnfs_stat = st;
    result = iget(sb, st->st_ino);
    tnfs_stat = NULL;
    up(&read_semaphore);

    return result;
}

void tnfs_read_inode(struct inode *i)
{
#ifdef DEBUG
    printk("tnfs_read_inode %i\n", (int) i->i_ino);
#endif

    if (tnfs_stat == NULL || tnfs_stat->st_ino != i->i_ino) {
	printk("tnfs_read_inode: called from bad point\n");
	return;
    }

    i->i_dev = i->i_sb->s_dev;

    if (i->i_ino == TNFS_ROOT_INO) {		/* superblock */
	i->i_mode = S_IFDIR | S_IRUGO | S_IXUGO | S_IWUSR;
	i->i_nlink = 2;
	i->i_uid = TNFS_SB(i->i_sb)->mnt->uid;
	i->i_gid = TNFS_SB(i->i_sb)->mnt->gid;
	i->i_size = 1024;
	i->i_atime = i->i_mtime = i->i_ctime = CURRENT_TIME;
    }
    else {
	i->i_mode = (mode_t)tnfs_stat->st_mode;
	i->i_nlink = (nlink_t)tnfs_stat->st_nlink;
	i->i_uid = (uid_t)tnfs_stat->st_uid;
	i->i_gid = (gid_t)tnfs_stat->st_gid;
	i->i_size = (off_t)tnfs_stat->st_size;
	i->i_atime = (time_t)tnfs_stat->st_atime;
	i->i_mtime = (time_t)tnfs_stat->st_mtime;
	i->i_ctime = (time_t)tnfs_stat->st_ctime;
    }
    i->i_rdev = 0;
    i->i_blksize = TNFS_BLKSIZE;
    i->i_blocks = ((i->i_size+TNFS_BLKSIZE-1) >> TNFS_BLKSIZE_BITS);
    TNFS_IINFO( i ) -> fd = -1;
    TNFS_IINFO( i ) -> mtime = 0;
    
    if (S_ISREG(i->i_mode))
	i->i_op = &tnfs_file_iops;
    else if (S_ISDIR(i->i_mode))
	i->i_op = &tnfs_dir_iops;
    else if (S_ISLNK(i->i_mode))
	i->i_op = &tnfs_symlink_iops;
    else
	i->i_op = NULL;
}
