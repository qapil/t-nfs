/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	dir.c
 */

#include <linux/fs.h>
#include <linux/string.h>
#include <linux/sched.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "dir.h"
#include "support.h"
#include "net.h"
#include "cache.h"
#include "inode.h"
#include "ioctl.h"
#include "namei.h"

void tnfs_renew_times(struct dentry *dentry)
{
    for (;;) {
	dentry->d_time = jiffies;
	if (dentry == dentry->d_parent)
	    break;
	dentry = dentry->d_parent;
    }
}

ssize_t tnfs_dir_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
#ifdef DEBUG
    printk("tnfs_dir_read: %s\n", filp->f_dentry->d_name.name);
#endif
    return -EISDIR;
}

int tnfs_dir_open(struct inode *dir, struct file *filp)
{
    struct dentry *dentry = filp->f_dentry;
    unsigned char * buf = TNFS_SB( dir->i_sb ) -> packet;
    short len;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_dir_open: %s\n", dentry->d_name.name);
#endif
    
    // proti zacykleni
    if (dentry->d_inode->i_ino == TNFS_ROOT_INO &&
	(dentry->d_name.len != 1 || dentry->d_name.name[0] != '/'))
	return -EACCES;

    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = len = tnfs_build_path( dentry, buf + 2 );
    if (len == -1) {
        tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }

    res = tnfs_request(TNFS_SB(dir->i_sb), TNFS_CMD_MTIME, 2+len);

    if ( res < 0 )
	error = res;
    else if (buf[0] > 1)
	error = -EIO;
    else if (buf[0] == 0) {
	/* mtime = 0  ->  zavola se initialise_dir_cache() */
	get_dir_cache(dir->i_dev, dir->i_ino, 0, NULL);
	error = -*(int *)(buf+1);
    }
    else
	TNFS_IINFO(dir)->mtime = *(unsigned*)(buf+1);

    tnfs_unlock(TNFS_SB(dir->i_sb));
    return error;
}

/* read content of filp->f_dentry */
int tnfs_readdir(struct file *filp, void *dirent, filldir_t filldir)
{
    struct dentry *dentry = filp->f_dentry;
    struct inode *dir = dentry->d_inode;
    struct dir_cache *dc;
    unsigned char * buf = TNFS_SB( dir->i_sb ) -> packet;
    char from_server;
    short len, i;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_readdir: %s,%s,f_pos=%d\n", dentry->d_parent->d_name.name,
	dentry->d_name.name, (int) filp->f_pos);
#endif
    
    if (! (dc = get_dir_cache(dir->i_dev, dir->i_ino, TNFS_IINFO(dir)->mtime, &from_server)))
	if (! (dc = add_dir_cache(dentry, TNFS_IINFO(dir)->mtime)))
	    return -EINVAL;

    if (from_server) {
	unsigned long ino;
	char name[MAXNAMELEN];
	
	tnfs_lock(TNFS_SB(dir->i_sb));
	
        *(short*)(buf) = (short)len = tnfs_build_path( dentry, buf + 2 );
        if (len == -1) {
	    tnfs_unlock(TNFS_SB(dir->i_sb));
	    return -ENAMETOOLONG;
	}

        res = tnfs_request(TNFS_SB(dir->i_sb), TNFS_CMD_READDIR, 2+len);

        i = 2;
        do {
	    if ( res < 0 )
	        error = res;
	    else if (buf[0] == 1) {
	        ino = *(unsigned long*)(buf+1);
	        len = *(short*)(buf+5);
		memcpy(name, buf+7, len);
		name[len] = '\0';
		add_dir_cache_entry( dc, i++, ino, len, name );
		res = tnfs_request(TNFS_SB(dir->i_sb), TNFS_CMD_READDIR, -1);
	    } else if (buf[0] > 2)
	        error = -EIO;
	    else if ( buf[0] == 0)
	        error = -*(int*)(buf+1);

	    if ( error ) {
		tnfs_unlock(TNFS_SB(dir->i_sb));
	        return error;
	    }
	} while (buf[0] != 2);
	
	tnfs_unlock(TNFS_SB(dir->i_sb));
    } 

    switch ((unsigned int) filp->f_pos)
    {
    case 0:
	if (filldir(dirent, ".", 1, 0, dir->i_ino) < 0)
	    return -1;
	filp->f_pos = 1;
    case 1:
	if (filldir(dirent, "..", 2, 1, dentry->d_parent->d_inode->i_ino) < 0)
	    return -1;
	filp->f_pos = 2;
    }
    
    while (1) {
	unsigned long ino;
	char name[MAXNAMELEN];
	
	if (get_dir_cache_entry(dc, filp->f_pos, &ino, &len, name))
	    break;
		
	if (! ino) {
	    struct qstr qname;
	    qname.name = name;
	    qname.len = len;
	    if (! (ino = find_inode_number(dentry, &qname)))
		ino = tnfs_invent_inos(1);
	}
	if (filldir(dirent, name, len, filp->f_pos, ino) < 0)
	    break;
	    
	filp->f_pos += 1;
    }
    
    return 0;
}

int tnfs_lookup(struct inode *dir, struct dentry *dentry)
{
    char * buf = TNFS_SB( dir->i_sb ) -> packet;
    int len, res, error;
    struct inode *inode;
    struct tnfs_stat st;
    
#ifdef DEBUG
    printk("tnfs_lookup: %s\n", dentry->d_name.name);
#endif
    if (dir == 0 || ! S_ISDIR(dir->i_mode))
	return -ENOTDIR;

    inode = NULL;
    
    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(short*)(buf) = (short)len = tnfs_build_path(dentry, buf+2);
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    res = tnfs_request(TNFS_SB(dir->i_sb), TNFS_CMD_LOOKUP, 6+len);
    
    if ( res < 0 ) {
	tnfs_unlock(TNFS_SB(dir->i_sb));	/* odpoved nam nepatri */
	return res;
    }
    if (buf[0] > 1) {				/* nebo neplatny navratovy kod */
	tnfs_unlock(TNFS_SB(dir->i_sb));	
	return -EIO;
    }
    if (buf[0] == 0) {				/* polozka nenalezena */
	tnfs_unlock(TNFS_SB(dir->i_sb));
	d_add(dentry, inode);
	return (error = -*(int*)(buf+1)) == -ENOENT ? 0 : error;
    }
        
    memcpy( &st, buf+1, sizeof( struct tnfs_stat ) );

    tnfs_unlock(TNFS_SB(dir->i_sb));

    error = -EACCES;
    inode = tnfs_iget(dir->i_sb, &st);
    if (inode) {
	d_add(dentry, inode);
	tnfs_renew_times(dentry);
	error = 0;
    }
    
    return error;
}

static struct file_operations tnfs_dir_ops = {
    NULL,				/* lseek - default */
    tnfs_dir_read,			/* read - bad */
    NULL,				/* write - bad */
    tnfs_readdir,			/* readdir */
    NULL,				/* poll - default */
    tnfs_ioctl,				/* ioctl */
    NULL,				/* mmap */
    tnfs_dir_open,			/* open - revalidate */
    NULL,				/* flush */
    NULL,				/* release - default */
    NULL,				/* fsync */
    NULL,				/* fasync */
    NULL,				/* check_media_change */
    NULL,				/* revalidate */
    NULL				/* lock */
};

struct inode_operations tnfs_dir_iops = {
    &tnfs_dir_ops,			/* default_dir_ops */
    tnfs_create,			/* create */
    tnfs_lookup,			/* lookup */
    tnfs_link,				/* link */
    tnfs_unlink,			/* unlink */
    tnfs_symlink,			/* symlink */
    tnfs_mkdir,				/* mkdir */
    tnfs_rmdir,				/* rmdir */
    NULL,				/* mknod */
    tnfs_rename,			/* rename */
    NULL,				/* readlink */
    NULL,				/* follow_link */
    NULL,				/* readpage */
    NULL,				/* writepage */
    NULL,				/* bmap */
    NULL,				/* truncate */
    NULL,				/* permission */
    NULL,				/* smap */
    NULL,				/* updatepage */
    NULL				/* revalidate */
};
