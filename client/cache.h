/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	cache.h - cache used for directory entries
 */
 
#ifndef	CACHE_H
#define	CACHE_H

#include <linux/types.h>

struct dentry;
struct cache_block;
struct dir_cache;

#ifdef DEBUG
void clear_all_dir_cache(void);
#endif
void clear_dir_cache(dev_t);
void initialise_dir_cache(struct dir_cache *);
void invalidate_dir_cache(struct dir_cache *);
int refill_dir_cache(struct dir_cache *, struct dentry *);
struct dir_cache *get_dir_cache(dev_t, long, unsigned, char *);
struct dir_cache *add_dir_cache(struct dentry *, unsigned);
int add_dir_cache_entry(struct dir_cache *, int, long, short, const char * );
int get_dir_cache_entry(struct dir_cache *, int, long *, short *, char * );

#endif	/* CACHE_H */
