/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	net.c
 */

#include <linux/kernel.h>
#include <linux/socket.h>
#include <net/sock.h>
#include <net/scm.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <asm/uaccess.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "net.h"
#include "support.h"
#include "cache.h"

/*
 * z nasi pridavne struktury suberbloku vytahne ukazatal na strukturu
 * pro sitovy soket, pres ktery komunikujeme se servrem
 */
static struct socket * server_socket( struct tnfs_sb_info * sb )
{
    struct file * file;
    
    if ( sb && ( file = sb -> sock_file ) )
    {
	// mohli bysme zkontrolovat, jestli je to spravny soket
	// pomoci tnfs_valid_socket( file -> f_dentry -> d_inode )
	return &file -> f_dentry -> d_inode -> u.socket_i;
    }
    return NULL;
}

/*
 * omrkne, jestli inode ukazuje na soket, s kterym mame co do cineni, tzn, 
 * ze chceme sitovy soket a pokud mozno TCP
 */
int tnfs_valid_socket( struct inode * inode )
{
    return ( inode && S_ISSOCK( inode -> i_mode ) &&
	     inode -> u.socket_i.type == SOCK_STREAM );
}

/*
 * Prijme ze soketu socket data delky size, ktera preda do bufferu ubuf.
 * Flagy muzou byt neco jako necekat, kdyz je tam malo dat, omrkni situaci ap.
 */
static int _recvfrom( struct socket * socket, unsigned char * ubuf,
		      int size, unsigned flags )
{
    struct iovec iov;		// z linux/uio.h, obsahuje odkaz na data/buffer,
				// z/do ktereho se data ziskaji/presunou
    struct msghdr msg;		// z linux/socket.h, ridici hlavicka
    struct scm_cookie scm;

    msg.msg_name = NULL;	// jmeno soketu
    msg.msg_namelen = 0;	// jeho delka
    msg.msg_iov = &iov;		// datove bloky
    msg.msg_iovlen = 1;		// jejich pocet
    msg.msg_control = NULL;	// zrejme pro kazdy protokol jinaci
    iov.iov_base = ubuf;	// konkretni blok na data
    iov.iov_len = size;		// jejich velikost
    
    memset( &scm, 0, sizeof( scm ) );
    size = socket -> ops -> recvmsg( socket, &msg, size, flags, &scm );
    if ( size >= 0 )
	scm_recv( socket, &msg, &scm, flags );
    return size;		// tolik jsme zrejme precetli
}

/*
 * Posle data do soketu bafru.
 */
static int _sendto( struct socket * socket, const void * buff, int len )
{
    struct iovec iov;		// sem ulozime adresu a velikost dat
    struct msghdr msg;		// ridici hlavicka
    struct scm_cookie scm;	// tohle mi jeste nedoslo, k cemu to slouzi
    int error;

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;		// blok dat
    msg.msg_iovlen = 1;		// predavame data v jednom bloku
    msg.msg_control = NULL;
    msg.msg_controllen = 0;
    iov.iov_base = (void *)buff;
    iov.iov_len = len;
    
    msg.msg_flags = 0;
    // kdyby ty zdrojaky byly lepe okomentovane..., viz net/core/scm.c
    error = scm_send( socket, &msg, &scm );
    if ( error >= 0 )
    {
	error = socket -> ops -> sendmsg( socket, &msg, len, &scm );
	scm_destroy( &scm );
    }
    return error;
}

/*
 * Tak tohle je ta fce, ktera precte syrova data, ktera k nam priputovala
 * do soketu.
 */
static int tnfs_receive_raw( struct socket * socket, unsigned char *target,
			     int length )
{
    int result;
    int already_read = 0;

#ifdef	DEBUG
//    printk( "tnfs_receive_raw: %i\n", length );
#endif
    
    while ( already_read < length )
    {
	result = _recvfrom( socket,
			    (void *)(target + already_read),
			    length - already_read, 0 );
	if ( result == 0 )
	{
	    return -EIO;
	}
	if ( result < 0 )
	{
	    printk( "tnfs_receive_raw: recvfrom error = %d\n", -result );
	    return result;
	}
	already_read += result;
    }
    return already_read;
}

/*
 * A tahle je zase posle.
 */
static int tnfs_send_raw( struct socket * socket, unsigned char * source,
			  int length )
{
    int result;
    int already_sent = 0;

#ifdef	DEBUG
//    printk( "tnfs_send_raw: %i\n", length );
#endif
    
    while( already_sent < length )
    {
	result = _sendto( socket,
			  (void *)(source + already_sent),
			  length - already_sent );
	if ( result == 0 )
	{
	    return -EIO;
	}
	if ( result < 0 )
	{
	    printk( "tnfs_send_raw: sendto error = %d\n", -result );
	    return result;
	}
	already_sent += result;
    }
    return already_sent;
}

/*
 * Zatim z toho mam divny pocit, tahle fce nikam nepredava data, spis se
 * uvnitr cyklu krouti tak dlouho, az vsechny data nacte, jako by se tu
 * delala spinava prace s nechtenymi daty, ty jsou jednoduse ztraceny.
 * Nakonec to vypada tak, ze server, kdyz se po nem nic nechce, tak posila
 * pakety s vyznamem, ze je na zivu.
 * Tenhle callback je vypnut v tnfs_request, kde se prvne posle dotaz a
 * pak se vyzvedne odpoved, takze to vypada kupodivu tak, ze server mlci,
 * dokud neni pozadan, aby promluvil, zajimave...
 */
static void tnfs_data_callback( struct sock * sk, int len )
{
    struct socket * socket = sk -> socket;
    unsigned char peek_buff[4];
    int result;
    mm_segment_t fs;		// jekesi oznaceni segmentu
    
#ifdef	DEBUG
    printk( "tnfs_data_callback()\n" );
    printk( "dev=%d, ino=%ld\n", socket -> inode -> i_dev, 
	socket -> inode -> i_ino );
#endif
    
    // tahle saskarna je mi zahadou, zrejme si nastavuju prava kernelu, jelikos
    // nastavenim fs na ds se oznacuji za kernel thread ( jedina jina varianta
    // je user thread )
    fs = get_fs();		// z asm/uacces get_fs() <-> (current->addr_limit)
    set_fs( get_ds() );
    
    while ( 1 )
    {
	result = -EIO;
	if ( sk -> dead )
	{
#ifdef	DEBUG
	    printk( "tnfs_data_callback: sock is dead!" );
#endif
	    break;
	}
    	// MSG_XXX jsou v linux/socket.h
	// MSG_DONTWAIT je neblokujici se IO, ale MSG_PEEK ?, kdo vi...
        result = _recvfrom( socket, (void *)peek_buff, 1,
			    MSG_PEEK | MSG_DONTWAIT );
	if ( result == -EAGAIN || peek_buff[0] != TNFS_CMD_TERMINATE )
	    break;

	result = _recvfrom( socket, (void *)peek_buff, 4, MSG_DONTWAIT );
	
#ifdef	DEBUG
	printk( "tnfs_callback: got TERMINATE\n" );
#endif

	if ( result == -EAGAIN )
	    break;

	socket->ops->shutdown( socket, 2 );
	sock_release( socket );
    }
    set_fs( fs );	// obnoveni stavu

    if ( result == -EAGAIN )
    {	// sched.h
	wake_up_interruptible( sk -> sleep );	// huh...
	// v co se tohle volani rozvine a co z toho vyleze...
    }
}

/*
 * Napojime se na soket, abysme odchytavali pakety od serveru, kterym se
 * rika keepalive.
 */
int tnfs_catch_terminate( struct tnfs_sb_info * sb )
{
    struct socket * socket;
    struct sock * sk;			// net/sock.h
    void * data_ready;
    int error = -EINVAL;
    
    socket = server_socket( sb );
    if ( !socket )
    {
	printk( "tnfs_catch_terminate: did not get valid server!" );
	sb -> data_ready = NULL;
	goto out;
    }
    
    sk = socket -> sk;
    if ( sk == NULL )
    {
	printk( "tnfs_catch_terminate: sk == NULL\n" );
	sb -> data_ready = NULL;
    }

    // nainstalujeme callback atomicky, prece si to nechceme rozhazet s
    // race conditions
    // xchg() je v asm/system.h, jenom hadam, ze vraci puvodni hodnotu
    // ze sk -> data_ready ( ten AT&T assembler je desnej, brrr, grrr )
    data_ready = xchg( &sk -> data_ready, tnfs_data_callback );
    if ( data_ready != tnfs_data_callback )
    {
	sb -> data_ready = data_ready;
	error = 0;
    } else	// jako bysme tu uz jednou byli
	printk( KERN_ERR "tnfs_keepalive: already done!\n" );
out:
    return error;
}

/*
 * Odpojime se od soketu a obnovime puvodni obsluhu
 */
int tnfs_dont_catch_terminate( struct tnfs_sb_info * sb )
{
    struct socket * socket;
    struct sock * sk;
    void * data_ready;
    int error = -EINVAL;

    socket = server_socket( sb );
    if ( !socket )
    {
#ifdef DEBUG
	printk( "tnfs_dont_catch_terminate: did not get valid server!\n" );
#endif
	return error;
    }
    
    sk = socket -> sk;
    if ( sk == NULL )
    {
#ifdef DEBUG
	printk( "tnfs_dont_catch_terminate: sk == NULL!\n" );
#endif
	return error;
    }
    
    if ( sb -> data_ready == NULL )
    {
#ifdef DEBUG
	printk( "tnfs_dont_catch_terminate: sb->data_ready == NULL\n" );
#endif
	return error;
    }

    // obnovime puvodni callback, opet atomicky
    data_ready = xchg( &sk -> data_ready, sb -> data_ready );
    if ( data_ready != tnfs_data_callback )
    {	// tohle je opravdu podivne... a zivotne dulezite
	printk( "tnfs_dont_catch_terminate: sk->data_callback != tnfs_data_callback\n" );
    }

    return 0;
}

/*
 * Mountovaci program preda cislo soketu, ktery vytvoril pro sitovou
 * komunikaci, ze struktury, kde jsou informace o otevrenych souboruech
 * (zrejme current), vytahne ukazatel na struct file, ktery si shovame
 * do svych informasi pripojenych k super bloku.
 * Jelikos nam mountovaci program predal odkaz na soket, pres strukturu
 * file se dostanem na operace na soketu, dobry ne?
 */
int tnfs_newconn( struct tnfs_sb_info * sb )
{
    struct file * filp;
    int error;

#ifdef	DEBUG
    printk( "tnfs_newconn: fd=%d, pid=%d\n", sb -> mnt -> server, current -> pid );
#endif

    error = -EBADF;
    // vyzvedneme si strukturu na soubor (v nasem pripade soket) i s operacema,
    // ktere na nem muzem provadet (tedy sitovani), viz linux/file.h
    filp = fget( sb -> mnt -> server );
    if ( !filp )
	return error;

    if ( !tnfs_valid_socket( filp -> f_dentry -> d_inode ) )
	fput( filp );
    else {
	sb -> sock_file = filp;
	tnfs_catch_terminate( sb );
	sb -> state = TNFS_CONN_VALID;
	error = 0;			// uff, mame to za sebou
    }

    return error;
}

int tnfs_terminate(struct tnfs_sb_info *sb)
{
    if (sb->state == TNFS_CONN_VALID) {
	mm_segment_t fs;
	
#ifdef DEBUG
	printk("tnfs_terminate\n");
#endif
	fs = get_fs();
	set_fs( get_ds() );
	tnfs_build_header(sb->packet, TNFS_CMD_TERMINATE, 0);
	tnfs_send_raw( server_socket( sb ), (void *)sb->packet, 4 );
	set_fs( fs );
    }
    kill_proc(sb->mnt->umnt_pid, SIGUSR1, 1);
    return 0;
}

/*
 * Precte prvni 4 byty doslych dat, ve kterych je informace o delce dat.
 */
static int tnfs_get_length( struct socket * socket, unsigned char * header )
{
    int result;
    unsigned char peek_buff[4];
    mm_segment_t fs;
    
re_recv:
    fs = get_fs();
    set_fs( get_ds() );
    result = tnfs_receive_raw( socket, peek_buff, 4 );
    set_fs( fs );
    
    if ( result < 0 )
    {
#ifdef	DEBUG
	printk( "tnfs_get_length: recv error = %d\n", -result );
#endif
	return result;
    }
    switch ( peek_buff[0] ) {
    case 0x85:	// covece, ta 0x85 je mozna nejaka informace od serveru,
#ifdef	DEBUG	// dava tak navedomi, ze je na prijmu, meybe
	printk( "tnfs_get_length: got SESSION KEEP ALIVE\n" );
#endif
	goto re_recv;
    default:
	if (peek_buff[0] <= TNFS_MAX_CMD)
	    break;
	    
#ifdef	DEBUG
	printk( "tnfs_get_length: Invalid packet, code=0x%x\n", peek_buff[0] );
	// tak tohle je jasny! Prvni byte paketu udava jaky druh paketu
	// je to zac a tim i jeho vnitrni strukturu! To bysme si taky meli
	// vymyslet vlastni pro nas tnfs :)
#endif
	return -EIO;
    }
    
    if ( header != NULL )	// posleme data vys
	memcpy( header, peek_buff, 4 );

    // delka RFC NB hlavicky je celkova delka, ale tesko rict, jestli se
    // i my podle toho budem ridit
    return tnfs_len( peek_buff );
}

/*
 * Nacte cely prisly paket. Kolik toho ma nacist se dozvi z prvnich ctyrech
 * bytech, ktere dorazi. Nacte to do sb -> packet, na ktery ma pristup
 * ze superbloku. Predpoklada se, ze sb neposle vic dat, nez kolik tam
 * klient ma naalokovane. Slusne chovani se totiz obcas vyzaduje, aby
 * se nekde neco nehryzlo
 */
static int tnfs_receive( struct tnfs_sb_info * sb, int cmd )
{
    struct socket * socket = server_socket( sb );
    unsigned char * packet = sb -> packet;
    int result;
    unsigned char peek_buff[4];

    result = tnfs_get_length( socket, peek_buff );
    if ( result < 0 ) return result;
    if ( peek_buff[0] != cmd ) return -EIO;
    
    // zacnem zaplnovat nas prostor pro prijem paketu
    result = tnfs_receive_raw( socket, packet, result );
#ifdef	DEBUG
    if ( result < 0 )
	printk( "tnfs_receive: receive error : %d\n", -result );
#endif

    return result;
}
        
/*
 * Posle uz pripraveny paket, ktery je v sb -> packet. Rovnou si pocka
 * na odpoved.
 */
int tnfs_request( struct tnfs_sb_info * sb, int cmd, int len )
{
    mm_segment_t fs;
    int result;
    unsigned char * buffer;
    unsigned char header[4];
    
    result = -EBADF;
    buffer = sb -> packet;
    if ( !buffer )
	goto bad_no_packet;	// nemame kam dat dosla data, to je zle

    result = -EIO;
    if ( sb -> state != TNFS_CONN_VALID )
	goto bad_no_conn;
	
    if ( (result = tnfs_dont_catch_terminate( sb )) != 0 )
	goto bad_conn;
	
    fs = get_fs();
    set_fs( get_ds() );

    if (len != -1) {	// -1 je spec hodnota pouzivana v READDIR
	tnfs_build_header( header, cmd, len );
	result = tnfs_send_raw( server_socket( sb ), header, 4 );
	if ( result == 4 )  
	    result = tnfs_send_raw( server_socket( sb ), buffer, len );
    }
    else result = 1;

    if ( result > 0 )
    {	// data byla uspesne odeslana, tak si pockame na odpoved
	result = tnfs_receive( sb, cmd );
    }
    /* read/write chyby jsou obslouzeny errno */
    
    set_fs( fs );

    if ( result >= 0 )
    {
	int result2 = tnfs_catch_terminate( sb );
	if ( result2 < 0 )
	{
	    printk( "tnfs_request: catch_terminate failed\n" );
	    result = result2;
	}
    }

    if ( result < 0 )
	goto bad_conn;

out:
    return result;

bad_conn:
#ifdef DEBUG
    printk( "tnfs_request: result %d, setting invalid\n", result );
#endif
    sb -> state = TNFS_CONN_INVALID;
    clear_dir_cache(sb->sb->s_dev);
    goto out;
bad_no_packet:
    printk( "tnfs_request: no packet!\n" );
    goto out;
bad_no_conn:
    printk( "tnfs_request: connection not valid\n" );
    goto out;
}
