/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	tnfs_fs.h
 */
 
#ifndef TNFS_FS_H
#define TNFS_FS_H

#define	TNFS_BLKSIZE_BITS	10
#define	TNFS_BLKSIZE		(1 << TNFS_BLKSIZE_BITS)
#define	TNFS_DIR_CACHE_SIZE	64
#define	TNFS_SUPER_MAGIC	0xBAFF

#define TNFS_SB(sb) 	((struct tnfs_sb_info *)&(sb->u))
#define	TNFS_IINFO(inode) ((struct tnfs_inode_info *)&(inode->u))

enum tnfs_conn_state {
    TNFS_CONN_VALID,	// vse je ok
    TNFS_CONN_INVALID,	// neco se pokazilo, ale jeste se nezkusilo 
			// obnovit spojeni
    TNFS_CONN_RETRIED	// pokus o obnovu spojeni, ale odmitnuto
};

struct tnfs_stat {
    long	st_ino;
    int		st_mode;
    int		st_nlink;
    int		st_uid;
    int		st_gid;
    long	st_size;
    long	st_atime;
    long	st_mtime;
    long	st_ctime;
};

struct tnfs_inode_info{
    int	fd;			// file descriptor na serveru
    unsigned mtime;		// cas zmeny na serveru
};

struct tnfs_sb_info {
    enum tnfs_conn_state state;
    struct tnfs_mount_data * mnt;
    struct file * sock_file;	// pristup na operace nad soketem
    
    struct semaphore sem;	// pri kazde souborove operaci se zamyka
    struct wait_queue *wait;
    struct super_block *sb;

    unsigned char * packet;	// misto se alokuje v tnfs_read_super()
    
    // pouzijem vlastni data_ready callback, ale potrebujeme original
    void * data_ready;
};

#include <asm/semaphore.h>
static inline void tnfs_lock( struct tnfs_sb_info *si) {
    down(&(si->sem));
}
static inline void tnfs_unlock( struct tnfs_sb_info *si) {
    up(&(si->sem));
}

#endif /* TNFS_FS_H */
