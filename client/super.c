/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	super.c
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/locks.h>
#include <linux/vmalloc.h>
#include <linux/string.h>
#include <asm/uaccess.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "super.h"
#include "inode.h"
#include "cache.h"
#include "net.h"
#include "namei.h"

static const struct super_operations tnfs_sops;
struct file_system_type tnfs_fs_type;

static void tnfs_put_super(struct super_block *s)
{
/* ukonci sitove spojeni */
    tnfs_dont_catch_terminate( TNFS_SB( s ) );
    tnfs_terminate(TNFS_SB(s));

/* uvolni alokovanou pamet */    
    clear_dir_cache(s->s_dev);
    vfree(TNFS_SB(s)->mnt);
    vfree(TNFS_SB(s)->packet);
    
#ifdef MODULE
    MOD_DEC_USE_COUNT;
#endif
#ifdef DEBUG
    printk("tnfs_put_super\n");
#endif
}

static int tnfs_statfs(struct super_block *s, struct statfs *sbuf, int bufsize)
{
    unsigned char * buf = TNFS_SB( s ) -> packet;
    struct statfs st;
    int res, error = 0;

#ifdef DEBUG
    printk("tnfs_statfs\n");
#endif

    memset(&st, 0, sizeof(st));
    
    tnfs_lock(TNFS_SB(s));
    
    res = tnfs_request(TNFS_SB(s), TNFS_CMD_STATFS, 0);

    if ( res < 0 )
	error = res;
    else if ( buf[0] > 1) 
	error = -EIO;
    else if (buf[0] == 0)
	error = -*(int *)(buf+1);
    
    tnfs_unlock(TNFS_SB(s));
    if (error) return error;

    st.f_type = TNFS_SUPER_MAGIC;
    st.f_bsize = TNFS_BLKSIZE;
    st.f_blocks = *(long*)(buf+1);
    st.f_bfree = *(long*)(buf+5);
    st.f_bavail = *(long*)(buf+9);
    st.f_files = -1;
    st.f_ffree = -1;
    st.f_namelen = MAXNAMELEN;

    tnfs_unlock(TNFS_SB(s));

    return copy_to_user(sbuf, &st, bufsize) ? -EFAULT : 0;
}

static struct super_block *tnfs_read_super(struct super_block *s, void *data, int silent)
{
    struct inode *root_inode = 0;
    struct dentry *root;
    struct tnfs_mount_data *mnt_data = (struct tnfs_mount_data *) data;
    struct tnfs_stat st;

#ifdef DEBUG
    printk("TNFS: tnfs_read_super()\n");
    if (mnt_data)
	printk("version: %i\nuid=%i\n", mnt_data->version, mnt_data->uid);
#endif

#ifdef MODULE
    MOD_INC_USE_COUNT;
#endif

    lock_super(s);

    if (s->s_root)
	goto out_unlock;
    s->s_blocksize = TNFS_BLKSIZE;
    s->s_blocksize_bits = TNFS_BLKSIZE_BITS;
    s->s_magic = TNFS_SUPER_MAGIC;
    s->s_op = (struct super_operations *) &tnfs_sops;
    s->s_root = NULL;
    
    /* Allocate the mount data structure */
    mnt_data = (struct tnfs_mount_data *) vmalloc( sizeof( struct tnfs_mount_data ) );;
    if (!mnt_data)
	goto out_no_mem;
	
    memcpy( mnt_data, data, sizeof( struct tnfs_mount_data ) );
    TNFS_SB(s)->mnt = mnt_data;
    
    TNFS_SB(s)->sock_file = NULL;
    TNFS_SB(s)->sem = MUTEX;
    TNFS_SB(s)->wait = NULL;
    TNFS_SB(s)->state = TNFS_CONN_INVALID;
    TNFS_SB(s)->packet = (char *)vmalloc(PACKETSIZE);
    TNFS_SB(s)->sb = s;
    
    unlock_super(s);

    st.st_ino = TNFS_ROOT_INO;
    root_inode = tnfs_iget(s, &st);
    root = d_alloc_root(root_inode, NULL);

    if (s->s_root)
	goto out_dput;
	
    if (! root)
	goto fail_iput;

    if (s->s_root)
	goto out_dec;
	
    lock_super(s);
    s->s_root = root;
    unlock_super(s);
    tnfs_newconn( TNFS_SB( s ) );

#ifdef DEBUG
    printk("sending root_dev=%li\n", (long)s->s_dev);
#endif
    *(unsigned*)(TNFS_SB(s)->packet) = s->s_dev;
    if (tnfs_request(TNFS_SB(s), TNFS_CMD_ROOTDEV, 4) < 0)
	goto out_dec;

    return s;
    
out_unlock:
    unlock_super(s);
    goto out_dec;

out_dput:
    if (root)
	dput(root);
    else
	iput(root_inode);

out_dec:
#ifdef MODULE
    MOD_DEC_USE_COUNT;
#endif
    s->s_dev = 0;
    return s;

out_no_mem:
    printk( "tnfs_read_super: allocation failure\n" );
    unlock_super(s);
    goto go_sure_out;
    
fail_iput:
    printk("tnfs_read_super failed\n");
    iput(root_inode);

go_sure_out:
#ifdef MODULE    
    MOD_DEC_USE_COUNT;
#endif
    return NULL;
}

static int init_tnfs_fs(void)
{
    return register_filesystem(&tnfs_fs_type);
}

#ifdef MODULE

int init_module(void)
{
#ifdef DEBUG
    printk("TNFS: init_module()\n");
#endif
    return init_tnfs_fs();
}

void cleanup_module(void)
{
#ifdef DEBUG
    printk("TNFS: cleanup_module()\n");
    clear_all_dir_cache();
#endif
    unregister_filesystem(&tnfs_fs_type);
}

#endif /* MODULE */

static const struct super_operations tnfs_sops =
{
    tnfs_read_inode,		/* read_inode */
    NULL,			/* write_inode */
    NULL,			/* put_inode - (vypadnuti z cache) optional */
    NULL,			/* delete_inode */
    tnfs_notify_change,		/* notify_change - (zmena atributu) kdyz NULL, tak bude write_inode */
    tnfs_put_super,		/* put_super - pred uvolnenim superbloku */
    NULL,			/* write_super - optional */
    tnfs_statfs,		/* statfs */
    NULL,			/* remount_fs */
    NULL,			/* clear_inode - optional */
    NULL			/* umount_begin */
};

struct file_system_type tnfs_fs_type =
{
    "tnfs",			/* name */
    0,				/* flags */
    tnfs_read_super,		/* read_super */
    NULL			/* next */
};
