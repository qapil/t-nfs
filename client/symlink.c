/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	symlink.c
 */

#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/malloc.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "symlink.h"
#include "support.h"
#include "net.h"

static int tnfs_readlink(struct dentry * dentry, char * dest, int dest_size )
{
    struct inode * dir = dentry -> d_inode;
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    short len;
    int result = 0;

#ifdef DEBUG
    printk("tnfs_readlink: %s\n", dentry->d_name.name);
#endif

    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(int*)(buf) = dest_size;
    *(short*)(buf+4) = len = tnfs_build_path( dentry, buf + 6 );
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return -ENAMETOOLONG;
    }
    
    result = tnfs_request( TNFS_SB( dir -> i_sb ), TNFS_CMD_READLINK, 6+len );
    
    if ( result >= 0 ) {
	if ( buf[0] > 1 )
	    result = -EIO;
	else if ( buf[0] == 0 )
	    result = -*(int*)(buf+1);
	else {
	    result = len = *(short*)(buf+1);
	    if ( copy_to_user( dest, buf + 3, len ) ) {
#ifdef DEBUG
		printk( "readlink : EFAULT \n" );
#endif
		result = -EFAULT;
	    }
	}
    }

    tnfs_unlock(TNFS_SB(dir->i_sb));
    return result;
}

static struct dentry * tnfs_follow_link(struct dentry * dentry, struct dentry * base,
				 unsigned int follow )
{
    struct inode * dir = dentry -> d_inode;
    unsigned char * buf = TNFS_SB( dir -> i_sb ) -> packet;
    char * path;
    short len;
    int res;
    struct dentry * result = NULL;
    
#ifdef DEBUG
    printk("tnfs_follow_link: %s, base: %s, follow: %i\n",
	    dentry->d_name.name, base->d_name.name, follow);
#endif

    tnfs_lock(TNFS_SB(dir->i_sb));
    
    *(int*)(buf) = MAXPATHLEN;
    *(short*)(buf+4) = len = tnfs_build_path( dentry, buf + 6 );
    if (len == -1) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return ERR_PTR(-ENAMETOOLONG);
    }
    
    res = tnfs_request( TNFS_SB( dir -> i_sb ), TNFS_CMD_READLINK, 6+len );

    if ( res < 0 )
	result = ERR_PTR( res );
    else if ( buf[0] > 1 ) 
	result = ERR_PTR( -EIO );
    else if ( buf[0] == 0 ) 
	result = ERR_PTR(-*(int*)(buf+1) );
    
    if ( result != NULL ) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	dput( base );
	return result;
    }
    
    len = *(short*)(buf+1);

    path = kmalloc( len+1, GFP_KERNEL );
    if ( !path ) {
	tnfs_unlock(TNFS_SB(dir->i_sb));
	return ERR_PTR(-ENOMEM);
    }

    memcpy(path, buf+3, len);
    path[len] = '\0';

    tnfs_unlock(TNFS_SB(dir->i_sb));
        
    result = lookup_dentry( path, base, follow );
    kfree( path );
    
    return result;
}

struct inode_operations tnfs_symlink_iops = {
    NULL,				/* default_symlink_ops */
    NULL,				/* create */
    NULL,				/* lookup */
    NULL,				/* link */
    NULL,				/* unlink */
    NULL,				/* symlink */
    NULL,				/* mkdir */
    NULL,				/* rmdir */
    NULL,				/* mknod */
    NULL,				/* rename */
    tnfs_readlink,			/* readlink */
    tnfs_follow_link,			/* follow_link */
    NULL,				/* readpage */
    NULL,				/* writepage */
    NULL,				/* bmap */
    NULL,				/* truncate */
    NULL,				/* permission */
    NULL,				/* smap */
    NULL,				/* updatepage */
    NULL				/* revalidate */
};
