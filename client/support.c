/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	support.c
 */
 
#include <linux/string.h>
#include <linux/fs.h>

#include "../tnfs.h"
#include "support.h"

void tnfs_build_header(BYTE *p, BYTE cmd, unsigned len)
{
     *p = cmd;
     *(p+1) = (len & 0xFF0000) >> 16;
     *(p+2) = (len & 0x00FF00) >> 8;
     *(p+3) = len & 0x0000FF;
}

/*
 * Vycte z bufferu jeho delku.
 */
unsigned tnfs_len( BYTE * p )
{
    return ((*(p+1) & 0x1) << 16L) | (*(p+2) << 8L) | *(p+3);
}

/*
 * hadej, co to dela
 */
static void reverse_string(char *buf, int len)
{
    char c;
    char *end = buf+len-1;

    while(buf < end) {
        c = *buf;
        *(buf++) = *end;
        *(end--) = c;
    }
}

/*
 * vytahne celou cestu 
 */
short tnfs_build_path( struct dentry * entry, char * buf )
{
    char *path = buf;
    int len = 0;

    if (entry == NULL) {
	*path = '\0';
        return 0;
    }

    /*
     * Build the path string walking the tree backward from end to ROOT
     * and store it in reversed order [see reverse_string()]
     */
    while( !IS_ROOT(entry) ) {
	len += entry->d_name.len;
	if (len >= MAXPATHLEN)
	    return -1;
        memcpy(path, entry->d_name.name, entry->d_name.len);
        reverse_string(path, entry->d_name.len);
        path += entry->d_name.len;

        entry = entry->d_parent;
        *(path++) = '/';
    }

    reverse_string(buf, path-buf);

    *(path++) = '\0';
    return (path-buf);
}
