/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	file.c
 */

#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/pagemap.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "file.h"
#include "support.h"
#include "net.h"

static inline void tnfs_unlock_page(struct page *page)
{
    clear_bit(PG_locked, &page->flags);
    wake_up(&page->wait);
}

static ssize_t tnfs_file_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
    struct inode *inode = file->f_dentry->d_inode;
    
#ifdef DEBUG
    printk("tnfs_file_read: %s, count=%i\n", file->f_dentry->d_name.name, count);
#endif

    if (!inode || TNFS_IINFO(inode)->fd == -1) {
	printk( "tnfs_file_read: bad inode\n" );
	return -ENOENT;
    }
    
    return generic_file_read( file, buf, count, ppos );
}

static ssize_t tnfs_file_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
    struct inode *inode = file->f_dentry->d_inode;
    int result;
    
#ifdef DEBUG
    printk("tnfs_file_write: %s, count=%i\n", file->f_dentry->d_name.name, count);
#endif

    if (!inode || TNFS_IINFO(inode)->fd == -1) {
	printk( "tnfs_file_write: bad inode\n" );
	return -ENOENT;
    }

    result = 0;
    if (count > 0)
	result = generic_file_write(file, buf, count, ppos);
    
    return result;
}

static int tnfs_file_open( struct inode * inode, struct file * file )
{
    unsigned char * buf = TNFS_SB( inode -> i_sb ) -> packet;
    short len;
    int res, error = 0;
    int mode = file->f_mode;
    
#ifdef	DEBUG
    printk( "tnfs_file_open: %s, count=%i, mode=%o\n", 
    file->f_dentry->d_name.name, file->f_dentry->d_count, file -> f_mode );
#endif

    mode = (mode-1) & 3;

    if ( !inode ) {
	printk( "tnfs_file_open: bad inode\n" );
	return -ENOENT;
    }

    if ( TNFS_IINFO( inode ) -> fd == -1 ) {
	tnfs_lock(TNFS_SB(inode->i_sb));

	*(int*)(buf) = mode;
	*(short*)(buf+4) = len = tnfs_build_path( file->f_dentry, buf+6 );
	if (len == -1) {
	    tnfs_unlock(TNFS_SB(inode->i_sb));
	    return -ENAMETOOLONG;
	}

	res = tnfs_request( TNFS_SB( inode -> i_sb ), TNFS_CMD_OPEN, len+6 );

	if ( res < 0 )
	    error = res;
	else if ( buf[0] > 1 )
	    error = -EIO;
	else if (buf[0] == 0)
	    error = -*(int*)(buf+1);
	else
	    TNFS_IINFO( inode ) -> fd = *(int*)(buf+1);
	
	tnfs_unlock(TNFS_SB(inode->i_sb));
    }
    return error;
}

static int tnfs_file_release( struct inode * inode, struct file * file )
{
    unsigned char * buf = TNFS_SB( inode -> i_sb ) -> packet;
    int res, error = 0;

#ifdef	DEBUG
    printk( "tnfs_file_release: %s, count=%i\n", file->f_dentry->d_name.name,
    file->f_dentry->d_count );
#endif

    if ( file -> f_dentry -> d_count == 1 ) {
	tnfs_lock(TNFS_SB(inode->i_sb));
	
	*(int*)(buf) = TNFS_IINFO( inode ) -> fd;
	res = tnfs_request( TNFS_SB(inode->i_sb), TNFS_CMD_CLOSE, 4 );

	if ( res < 0 )
	    error = res;
	else if ( buf[0] > 1 )
	    error = -EIO;
	else if (buf[0] == 0)
	    error = -*(int*)(buf+1);
	else
	    TNFS_IINFO( inode ) -> fd = -1;

	tnfs_unlock(TNFS_SB(inode->i_sb));
    }
    
    return error;
}


static int tnfs_proc_read(struct dentry * dentry, int offset, int rsize, char * dest )
{
    struct inode * inode = dentry -> d_inode;
    unsigned char * buf = TNFS_SB( inode -> i_sb ) -> packet;
    int res, result = 0;
    
    tnfs_lock(TNFS_SB(inode->i_sb));
    
    *(int*)(buf) = TNFS_IINFO( inode ) -> fd;
    *(int*)(buf+4) = offset;
    *(int*)(buf+8) = rsize;
    
    res = tnfs_request( TNFS_SB( inode -> i_sb ), TNFS_CMD_READ, 12 );
    
    if ( res < 0 )
	result = res;
    else if ( buf[0] == 1 )
	memcpy( dest, buf+5, result = *(int*)(buf+1) );
    else if ( buf[0] == 0 )
	result = -*(int*)(buf+1);
    else result = -EIO;

    tnfs_unlock(TNFS_SB(inode->i_sb));
    return result;
}

static int tnfs_proc_write(struct dentry * dentry, int offset, int wsize, char *src )
{
    struct inode *inode = dentry -> d_inode;
    unsigned char *buf = TNFS_SB(inode->i_sb)->packet;
    int res, result = 0;
    
    tnfs_lock(TNFS_SB(inode->i_sb));
    
    *(int*)(buf) = TNFS_IINFO(inode)->fd;
    *(int*)(buf+4) = offset;
    *(int*)(buf+8) = wsize;
    memcpy(buf+12, src, wsize);

    res = tnfs_request(TNFS_SB(inode->i_sb), TNFS_CMD_WRITE, 12 + wsize);
    
    if ( res < 0 )
	result = res;
    else if (buf[0]  > 1)
	result = -EIO;
    else {
	result = *(int*)(buf+1);		// delka nebo kod chyby
	if (buf[0] == 0)
	    result = -result;		// kod chyby
    }

    tnfs_unlock(TNFS_SB(inode->i_sb));
    return result;
}


static int tnfs_readpage_sync(struct dentry *dentry, struct page *page)
{
    char *buffer = (char *) page_address(page);
    unsigned long offset = page->offset;
    int rsize = PACKETSIZE;
    int count = PAGE_SIZE;
    int result;

    clear_bit(PG_error, &page->flags);

#ifdef DEBUG
    printk("tnfs_readpage_sync: file %s, count=%d@%ld, rsize=%d\n",
	    dentry->d_name.name, count, offset, rsize);
#endif

    do {
	if (count < rsize)
	    rsize = count;

	result = tnfs_proc_read(dentry, offset, rsize, buffer);
	if (result < 0)
	    goto io_error;

	count -= result;
	offset += result;
	buffer += result;
	dentry->d_inode->i_atime = CURRENT_TIME;
	if (result < rsize)
	    break;
    } while (count);

    memset(buffer, 0, count);
    set_bit(PG_uptodate, &page->flags);
    result = 0;

io_error:
    tnfs_unlock_page(page);
    return result;
}

static int tnfs_writepage_sync(struct dentry *dentry, struct page *page,
				unsigned long offset, unsigned int count)
{
    char *buffer = (char *) page_address(page) + offset;
    int wsize = PACKETSIZE;
    int result, written = 0;

    offset += page->offset;
    
#ifdef DEBUG
    printk("tnfs_writepage_sync: file %s, count=%d@%ld, wsize=%d\n",
	    dentry->d_name.name, count, offset, wsize);
#endif

    do {
	if (count < wsize)
	    wsize = count;

	result = tnfs_proc_write(dentry, offset, wsize, buffer);
	if (result < 0)
	    break;

	buffer += wsize;
	offset += wsize;
	written += wsize;
	count -= wsize;
	dentry->d_inode->i_mtime = dentry->d_inode->i_atime = CURRENT_TIME;
    } while (count);
    
    return written ? written : result;
}

static int tnfs_readpage( struct file * file, struct page * page )
{
    int error;

#ifdef	DEBUG
    printk( "tnfs_read_page: %s\n", file->f_dentry->d_name.name );
#endif

    set_bit(PG_locked, &page->flags);
    atomic_inc(&page->count);
    error = tnfs_readpage_sync(file->f_dentry, page);
    free_page(page_address(page));
    return error;
}

static int tnfs_writepage( struct file * file, struct page * page )
{
    int error;

#ifdef	DEBUG
    printk( "tnfs_write_page: %s\n", file->f_dentry->d_name.name );
#endif

    set_bit(PG_locked, &page->flags);
    atomic_inc(&page->count);
    error = tnfs_writepage_sync(file->f_dentry, page, 0, PAGE_SIZE);
    tnfs_unlock_page(page);
    free_page(page_address(page));
    return error;
}

static int tnfs_updatepage(struct file *file, struct page *page,
			    unsigned long offset, unsigned int count, int sync)
{
#ifdef DEBUG
    printk("tnfs_updatepage: %s, count=%d@%ld, sync=%d\n", 
	    file->f_dentry->d_name.name, count, page->offset+offset, sync);
#endif

    return tnfs_writepage_sync(file->f_dentry, page, offset, count);
}			    

static struct file_operations tnfs_file_ops = {
    NULL,				/* llseek */
    tnfs_file_read,			/* read */
    tnfs_file_write,			/* write */
    NULL,				/* readdir */
    NULL,				/* poll */
    NULL,				/* ioctl */
    generic_file_mmap,			/* mmap */
    tnfs_file_open,			/* open */
    NULL,				/* flush */
    tnfs_file_release,			/* release */
    NULL/*file_sync*/,				/* fsync */
    NULL,				/* fasync */
    NULL,				/* check_media_change */
    NULL,				/* revalidate */
    NULL				/* lock */
};

struct inode_operations tnfs_file_iops = {
    &tnfs_file_ops,			/* default_file_ops*/
    NULL,				/* create */
    NULL,				/* lookup */
    NULL,				/* link */
    NULL,				/* unlink */
    NULL,				/* symlink */
    NULL,				/* mkdir */
    NULL,				/* rmdir */
    NULL,				/* mknod */
    NULL,				/* rename */
    NULL,				/* readlink */
    NULL,				/* follow_link */
    tnfs_readpage,			/* readpage */
    tnfs_writepage,			/* writepage */
    NULL,				/* bmap */
    NULL,				/* truncate */
    NULL,				/* permission */
    NULL,				/* smap */
    tnfs_updatepage,			/* updatepage */
    NULL				/* revalidate */
};
