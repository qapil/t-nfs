/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	net.h
 */
 
#ifndef	NET_H
#define	NET_H

struct tnfs_sb_info;
struct inode;

int tnfs_newconn( struct tnfs_sb_info * );
int tnfs_terminate( struct tnfs_sb_info *);
int tnfs_valid_socket( struct inode * inode );
int tnfs_request( struct tnfs_sb_info *, int, int );
int tnfs_catch_terminate( struct tnfs_sb_info * );
int tnfs_dont_catch_terminate( struct tnfs_sb_info * );

#endif	/* NET_H */
