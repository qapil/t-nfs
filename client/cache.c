/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *      cache.c - cache used for directory entries
 */

#include <linux/mm.h>
#include <linux/string.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "cache.h"

#define CB_SIZE (2*sizeof(int)+2*sizeof(void *))

struct dir_cache_entry {
    long ino;
    short len;
    char *name;
};

struct cache_block {
    int entries;
    int free_size;
    char *names;
    struct cache_block *next;
    struct dir_cache_entry entry[(PAGE_SIZE-CB_SIZE) / sizeof(struct dir_cache_entry)];
};

struct dir_cache {
    dev_t dev;
    long ino;
    int total;
    unsigned mtime;
    unsigned used;
    struct cache_block *block;
};

static struct dir_cache cache[TNFS_DIR_CACHE_SIZE];

#ifdef DEBUG
/* free all cache - called before clean_module only for debug */
void clear_all_dir_cache(void)
{
    int i;
    
    for (i=0; i < TNFS_DIR_CACHE_SIZE; i ++)
	if (cache[i].block)
	    printk("WARNING: tnfs_dir_cache not cleaned\n");
}
#endif

/* free cache used by ... - called before umount */
void clear_dir_cache(dev_t dev)
{
    int i;
    
    for (i=0; i < TNFS_DIR_CACHE_SIZE; i ++)
	if (cache[i].dev == dev)
	    initialise_dir_cache(&cache[i]);
}

/* free all cache block and initialise dir cache as not used */
void initialise_dir_cache(struct dir_cache *dirc)
{
    if (dirc == NULL || dirc->ino == 0)
	return;
	
#ifdef DEBUG
    printk("initialise_dir_cache: dev=%i, ino=%li\n", dirc->dev, dirc->ino);
#endif

    invalidate_dir_cache(dirc);
    dirc->ino = 0;
    dirc->mtime = 0;
}

/* clear all directory entries from given dir_cache and free cache blocks*/
void invalidate_dir_cache(struct dir_cache *dirc)
{
    struct cache_block *block;
    
    if (dirc == NULL) return;
	
#ifdef DEBUG
    printk("invalidate_dir_cache: dev=%i, ino=%li\n", dirc->dev, dirc->ino);
#endif

    dirc->total = 0;
    while (dirc->block) {
	block = dirc->block;
	dirc->block = block->next;
	free_page((unsigned long) block);
    }
    dirc->block = NULL;
}

/* reread directory entries from server to cache */
int refill_dir_cache(struct dir_cache *dirc, struct dentry *dentry)
{
    return 0;
}

/* find directory in cache */
struct dir_cache *get_dir_cache(dev_t dev, long ino, unsigned mtime, char *invalid)
{
    int i;
    
#ifdef DEBUG
    printk("get_dir_cache: dev=%i, ino=%li\n", dev, ino);
#endif
    if (invalid) *invalid = 1;

    for (i = 0; i < TNFS_DIR_CACHE_SIZE; i ++) {
        if (cache[i].dev == dev && cache[i].ino == ino) {
	    if (cache[i].mtime != mtime) {
		initialise_dir_cache(&cache[i]);
		break;
	    }
	    if (invalid) *invalid = 0;
	    cache[i].used = CURRENT_TIME;
	    return &cache[i];
    	}
    }
    return NULL;
}

/* add directory to cache */
struct dir_cache *add_dir_cache(struct dentry *dentry, unsigned mtime)
{
    int i, empty = 0;
    unsigned long mt = CURRENT_TIME;
    
#ifdef DEBUG
    printk("add_dir_cache: dev=%i, ino=%li, name=%s\n", 
	    dentry->d_inode->i_dev, dentry->d_inode->i_ino, dentry->d_name.name);
#endif

    for (i = 0;  i < TNFS_DIR_CACHE_SIZE; i ++) {
	if (cache[i].ino == 0) {
	    empty = i;
	    goto add;
	}
	if (cache[i].mtime <= mt) {
	    mt = cache[i].mtime;
	    empty = i;
	}
    }
    invalidate_dir_cache(&cache[empty]);
add:
    cache[empty].dev = dentry->d_inode->i_dev;
    cache[empty].ino = dentry->d_inode->i_ino;
    cache[empty].mtime = mtime;
    cache[empty].used = CURRENT_TIME;
    cache[empty].block = NULL;
    return &cache[empty];
}

/* add directory entry to directory cache */
int add_dir_cache_entry(struct dir_cache *dirc, int pos,
			    long ino, short len, const char *name)
{
    struct cache_block *block;
    
    if (dirc == NULL || dirc->ino == 0) return -1;

    block = dirc->block;
    while (block && block->next)
	block = block->next;
    
    if (block == NULL) {
	block = (struct cache_block *) get_free_page(GFP_KERNEL);
	if (! block)
	    return -1;
	dirc->block = block;
	block->free_size = PAGE_SIZE-CB_SIZE;
	block->names = (char *)block + PAGE_SIZE;
    }
    else if (block->free_size < sizeof(struct dir_cache_entry)+len) {
	    block->next = (struct cache_block *) get_free_page(GFP_KERNEL);
	    if (! block)
		return -1;
	    block = block->next;
	    block->free_size = PAGE_SIZE-CB_SIZE;
	    block->names = (char *)block + PAGE_SIZE;
	}
    // there is no need to fill dirc->next, couse kernel zeroes hole page 
    dirc->total += 1;
    dirc->used = CURRENT_TIME;
    block->free_size -= sizeof(struct dir_cache_entry)+len;
    block->names -= len;
    block->entry[block->entries].ino = ino;
    block->entry[block->entries].len = len;
    block->entry[block->entries].name = block->names;
    memcpy(block->entry[block->entries].name, name, len);
    block->entries += 1;
    
    return 0;
}

/* get directory entry from directory cache */
int get_dir_cache_entry(struct dir_cache *dirc, int pos, 
			    long *ino, short *len, char *name)
{
    struct cache_block *block;
    
    if (dirc == NULL || dirc->ino == 0) return -1;

    pos -= 2;
    if (pos < 0 || pos >= dirc->total) return -1;
	
    block = dirc->block;
    while (block && pos >= block->entries) {
	pos -= block->entries;
	block = block->next;
    }
    
    if (block == NULL) return -1;

    dirc->used = CURRENT_TIME;	
    *ino = block->entry[pos].ino;
    *len = block->entry[pos].len;
    memcpy(name, block->entry[pos].name, *len);

    return 0;
}
