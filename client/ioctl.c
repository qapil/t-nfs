/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	ioctl.c
 */
 
#include <asm/ioctl.h>
#include <asm/uaccess.h>

#include "../tnfs.h"
#include "tnfs_fs.h"
#include "ioctl.h"

/*
 * vyloudi z inodu ukazatel na tnfs_sb_info
 */
#define	TNFS_SERVER( inode )	(struct tnfs_sb_info *)(&(inode->i_sb->u.generic_sbp))


int tnfs_ioctl( struct inode * inode, struct file * filep,
		unsigned int cmd, unsigned long arg )
{
    struct tnfs_sb_info * sb = TNFS_SERVER( inode );
    int result = -EINVAL;

    switch ( cmd ) {
    case TNFS_IOC_GETMOUNTUID:
	// odmountovaci program si muze hlidat vlastnika
#ifdef	DEBUG
	printk( "tnfs_ioctl:TNFS_IOC_GETMOUNTUID\n");
#endif
	result = put_user( sb -> mnt -> uid, (int *)arg );
	break;
    default:
    }
    
    return result;

}
