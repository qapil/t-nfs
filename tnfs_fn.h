/*
 *	             T N F S
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	tnfs_fs.h - inline network functions for server and mount utils
 */

#ifndef TNFS_FN_H
#define TNFS_FN_H

#include <unistd.h>

static inline int read_sock(int sock, void *buf, int len)
{
    int i, j=0;
    do	if ((i=read(sock, ((char *)buf)+j, len-j)) < 0)
	    return i;
    while ((j+=i) < len);
    return j;
}

static inline int write_sock(int sock, void *buf, int len)
{
    int i, j=0;
    do	if ((i=write(sock, ((char *)buf)+j, len-j)) < 0)
	    return i;
    while ((j+=i) < len);
    return j;
}

static inline void send_packet(int sock, unsigned char cmd, void *buf, int len)
{
    unsigned char *b = buf;
    
    b[0] = cmd;
    b[1] = (len & 0xFF0000) >> 16;
    b[2] = (len & 0x00FF00) >> 8;
    b[3] = len & 0x0000FF;
    write_sock( sock, buf, len + 4 );
}

#endif /* TNFS_FN_H */
