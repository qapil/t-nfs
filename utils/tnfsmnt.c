/*
 *	        T N F S - tnfsmnt
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	tnfsmnt.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <mntent.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <netdb.h>
#include <signal.h>

#include "../tnfs.h"
#include "../tnfs_fn.h"

char host[MAXHOSTNAMELEN+1],
     user[MAXUSERNAMELEN+1],
     pass[MAXPASSWORDLEN+1],
     mnt_point[MAXPATHLEN+1];
int port;

int server;
char send_terminate;
struct hostent * hostentry;


/***************************************/

void usage(char *msg)
{
    fprintf(stderr, "error: %s\n\n", msg);
    fprintf(stderr, "   usage: tnfsmount -host hostname [-port port_no] [-user username]\n");
    fprintf(stderr, "                    [-pass password] mount_point\n");
    exit(-1);
}

void error(char *msg)
{
    fprintf(stderr, "error: %s\n", msg);
    exit(-2);
}

/***************************************/

int test_mnt_point(struct stat *st)
{
    if (!S_ISDIR(st->st_mode)) {
	errno = ENOTDIR;
	return -1;
    }
    
    if (getuid() != 0					// nejsem root
	&& (getuid() != st->st_uid 			// adresar neni muj
	    || (st->st_mode & S_IRWXU) != S_IRWXU)) {	// nemam prava rwx
	errno = EPERM;
	return -1;	
    }
    
    return 0;
}

/***************************************/

#ifndef HAVE_KERNELD

int load_tnfs(void)
{
    FILE *fp;
    char s[1024], *p=0;
    pid_t pid;
    int status;

/* check for module */

    if ((fp = fopen("/proc/filesystems", "r")) == NULL) {
	perror("\"/proc/filesystems\" could not be read");
	return -1;
    }
    while (! feof(fp))
	if (fgets(s, sizeof(s), fp))
	    if ((p = strstr(s, "tnfs")) != NULL) break;
    fclose(fp);
    if (p) return 0;

/* load module */

    if ((pid = fork()) < 0) return -1;
    else
	if (pid == 0) {
	    execl("/sbin/modprobe", "modprobe", "tnfs", NULL);
	    _exit(127); /* execl error */
	}
	else 
	    while (waitpid(pid, &status, 0) < 0)
		if (errno != EINTR) {
		    status = -1;
		    break;
		}

    return status;
}

#endif

/***************************************/

static char *full_path(const char *p)
{
    char path[MAXPATHLEN];

    if (strlen(p) > MAXPATHLEN-1) return NULL;
    if (realpath(p, path) == NULL) return strdup(p);
    return strdup(path);
}

/***************************************/

static void usr_handler( int sig )
{
    return;
}

static int daemonize( void )
{
    int i;
    
    if ( (i = fork()) < 0 )
	printf( "Could not fork.\n" );

    if ( i == 0 ) {
//	char buf[4];
	
	signal( SIGUSR1, usr_handler );
	pause();
	
//	while (read(server, buf, 1) > 0) ;		// vyprazdni buffer
	shutdown( server, 2 );
	close(server);
#ifdef DEBUG    
	printf("connection closed\n");
	perror("close");
#endif
	exit(0);
    }
    return i;
}

int open_socket( void )
{
    struct sockaddr_in  sock_out;
    int res;

#ifdef DEBUG    
    printf( "Opening socket\n" );
#endif
    
    res = socket( PF_INET, SOCK_STREAM, 0 );
    if ( res == -1 ) {
	printf( "socket() error\n" );
	return -1;
    }
    
    memset( &sock_out, 0, sizeof( sock_out ) );
    memcpy( &sock_out.sin_addr, hostentry -> h_addr, hostentry -> h_length );
    sock_out.sin_family = AF_INET;
    sock_out.sin_port = port;
    
    // a konecne se pripojime
        
    if ( connect( res, (struct sockaddr *)&sock_out, sizeof( sock_out ) ) < 0 ) {
	printf( "Errror connectiog to %s\n", host );
	return -1;
    }
    
    return res;
}

int do_auth(int sock, char *user, char *pass)
{
    struct tnfs_auth auth;
    char buf[sizeof(auth)+4];
    
    auth.uid = getuid();
    auth.gid = getuid();
    auth.nobody_uid = 99;
    auth.nobody_gid = 99;
    strcpy(auth.user, user);
    strcpy(auth.pass, pass);

    *(struct tnfs_auth*)(buf+4) = auth;
    send_packet(sock, TNFS_CMD_AUTH, buf, 4+sizeof(auth));

    if (read_sock(sock, buf, 4) < 0)
	error("can't read auth reply");
	
    return (buf[0] == TNFS_CMD_OK);
}

/***************************************/

int main(int argc, char *argv[])
{
    struct stat st;
    unsigned long flags;
    struct tnfs_mount_data mnt_data;
    int fd;
    FILE *mtab;
    struct mntent ment;
    pid_t pid;

    printf("tnfsmount v. 0.01\n");
    
    host[0] = user[0] = pass[0] = '\0';
    port = TNFS_PORT;
    
    while (*(++argv) && **argv == '-') {
	if (strcmp(++*argv, "host") == 0) {
	    strncpy(host, *(++argv), MAXHOSTNAMELEN);
	    host[MAXHOSTNAMELEN] = '\0';
	}
	else if (strcmp(*argv, "port") == 0)
	    port = atoi(*(++argv));
	else if (strcmp(*argv, "user") == 0) {
	    strncpy(user, *(++argv), MAXUSERNAMELEN);
	    user[MAXUSERNAMELEN] = '\0';
	}
	else if (strcmp(*argv, "pass") == 0) {
	    strncpy(pass, *(++argv), MAXPASSWORDLEN);
	    pass[MAXPASSWORDLEN] = '\0';
	}
	else usage("unknown option");
    }
    if (*argv == NULL) usage("no mount point");
    strncpy(mnt_point, *argv, MAXPATHLEN);
    if (*(++argv)) usage("unknown option");
    if (host[0] == '\0') usage("no host");
    
    while (user[0] == '\0') {
	printf("username: ");
	fgets(user, MAXUSERNAMELEN, stdin);
	user[strlen(user)-1] = '\0';
    }
    if (pass[0] == '\0') {
	char *pw = getpass("password: ");
	if (strlen(pw) > MAXPASSWORDLEN) error("password to long\n");
	strcpy(pass, pw);
    }
    
/***************************************/
    
    if (geteuid() != 0) error("mount program must be suid root");
    
    if (stat(mnt_point, &st) == -1) error("could not find mount point");
    if (test_mnt_point(&st) != 0) error("bad mount point");
 
#ifndef HAVE_KERNELD
    if (load_tnfs() != 0) error("could not load tnfs file system");
#endif

    if ( (hostentry = gethostbyname( host )) == 0 )
	error( "Can't resolve host name" );
	
    server = open_socket();
    if ( server == -1 ) return -1;
    
    if (!do_auth(server, user, pass)) {
	printf("authentification FAILED\n");
	return -1;
    }
#ifdef DEBUG    
    else {
	printf("authentification OK\n");
    }
#endif
	
    pid = daemonize();
    
    mnt_data.version = TNFS_VERSION;
    mnt_data.uid = getuid();
    mnt_data.gid = getgid();
    mnt_data.server = server;
    mnt_data.umnt_pid = pid;
    
    flags = MS_MGC_VAL | MS_NODEV;
    if (mount(NULL, mnt_point, "tnfs", flags, &mnt_data) < 0) {
	error("mount");
	return(-1);
    }
    
    if ((fd = open(MOUNTED"~", O_RDWR|O_CREAT|O_EXCL, 0600)) == -1)
	error("can't get "MOUNTED"~ lock file");
    close(fd);

    if ((mtab = setmntent(MOUNTED, "a+")) == NULL)
	error("can't open "MOUNTED);

    ment.mnt_fsname = host;
    ment.mnt_dir = full_path(mnt_point);
    ment.mnt_type = "tnfs";
    ment.mnt_opts = "rw";
    ment.mnt_freq = 0;
    ment.mnt_passno = 0;

    if (addmntent(mtab, &ment) == 1)
	error("can't write mount entry");

    if (fchmod(fileno(mtab), 0644) == -1)
	error("can't set perms on "MOUNTED);
    endmntent(mtab);

    if (unlink(MOUNTED"~") == -1)
	error("can't remove "MOUNTED"~");


#ifdef DEBUG    
    printf("host: %s\nport: %i\nusername: %s\npassword: %s\nmnt_point: %s\n", 
	host, port, user, pass, mnt_point);
#endif

    return 0;
}
