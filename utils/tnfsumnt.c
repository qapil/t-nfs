/*
 *	       T N F S - tnfsumnt
 *	  -----------------------------
 *	   Trivial Nerwork File System
 *	-----------------------------------
 *	       Copyright (C) 1999
 *
 *	Jiri Kuhn	(jiri.kuhn@st.mff.cuni.cz)
 *	Martin Kvapil	(m.kvapil@email.cz)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *	tnfsumnt.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/param.h>
#include <fcntl.h>
#include <mntent.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include "../tnfs.h"

static int umount_ok(const char *mnt_point)
{
        int fid = open(mnt_point, O_RDONLY, 0);
        uid_t mnt_uid;

        if (fid == -1) {
                fprintf(stderr, "Could not open %s: %s\n",
                        mnt_point, strerror(errno));
                return -1;
        }
        
        if (ioctl(fid, TNFS_IOC_GETMOUNTUID, &mnt_uid) != 0) {
                fprintf(stderr, "%s probably not tnfs-filesystem\n", mnt_point);
                return -1;
        }

        if ((getuid() != 0) && (getuid() != (mnt_uid & 0xFFFF))) {
                fprintf(stderr, "You are not allowed to umount %s\n", mnt_point);
		fprintf(stderr, "\t%s was mounted by uid=%x\n", mnt_point, mnt_uid);
                return -1;
        }

        close(fid);
        return 0;
}

int main(int argc, char *argv[])
{
        char mnt_point[MAXPATHLEN+1];
        int fd;

        struct mntent *mnt;
        FILE* mtab;
        FILE* new_mtab;

        if (argc != 2) {
                exit(1);
        }

        if (geteuid() != 0) {
                fprintf(stderr, "%s must be installed suid root\n", argv[0]);
                exit(1);
        }

	if (realpath(argv[1], mnt_point) == NULL)
	    strncpy(mnt_point, argv[1], MAXPATHLEN);
	mnt_point[MAXPATHLEN] = '\0';

	if (mnt_point[0] == '\0') exit(1);
        if (umount_ok(mnt_point) != 0) exit(1);
        if (umount(mnt_point) != 0) {
                fprintf(stderr, "Could not umount %s: %s\n",
                        mnt_point, strerror(errno));
                exit(1);
        }

        if ((fd = open(MOUNTED"~", O_RDWR|O_CREAT|O_EXCL, 0600)) == -1)
        {
                fprintf(stderr, "Can't get "MOUNTED"~ lock file");
                return 1;
        }
        close(fd);
	
        if ((mtab = setmntent(MOUNTED, "r")) == NULL) {
                fprintf(stderr, "Can't open " MOUNTED ": %s\n",
                        strerror(errno));
                return 1;
        }

#define MOUNTED_TMP MOUNTED".tmp"

        if ((new_mtab = setmntent(MOUNTED_TMP, "w")) == NULL) {
                fprintf(stderr, "Can't open " MOUNTED_TMP ": %s\n",
                        strerror(errno));
                endmntent(mtab);
                return 1;
        }

        while ((mnt = getmntent(mtab)) != NULL) {
                if (strcmp(mnt->mnt_dir, mnt_point) != 0) {
                        addmntent(new_mtab, mnt);
                }
        }

        endmntent(mtab);

        if (fchmod (fileno (new_mtab), S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH) < 0) {
                fprintf(stderr, "Error changing mode of %s: %s\n",
                        MOUNTED_TMP, strerror(errno));
                exit(1);
        }

        endmntent(new_mtab);

        if (rename(MOUNTED_TMP, MOUNTED) < 0) {
                fprintf(stderr, "Cannot rename %s to %s: %s\n",
                        MOUNTED, MOUNTED_TMP, strerror(errno));
                exit(1);
        }

        if (unlink(MOUNTED"~") == -1)
        {
                fprintf(stderr, "Can't remove "MOUNTED"~");
                return 1;
        }

	return 0;
}	
